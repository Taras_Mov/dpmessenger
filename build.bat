del __delivery.zip
rmdir /s /q __delivery
mkdir __delivery
mkdir __delivery\DPMessengerService.Service
mkdir __delivery\DPMessengerService.WebService
mkdir __delivery\DPMessenger.UI
xcopy DPMessenger.Service.ConsoleHost\bin\Debug\*.* __delivery\DPMessengerService.Service /y
xcopy DPMessenger.Service.ServerHost\bin\Debug\*.* __delivery\DPMessengerService.Service /y
xcopy DPMessengeR.UI\bin\Debug\*.* __delivery\DPMessenger.UI /y
xcopy schema.sql __delivery\ /y
xcopy for webconfig of appconfig.txt __delivery\ /y
del __delivery\DPMessengerService.Service\*.ServerHost.Install*
xcopy DpMessengerWebServiceTest\*.* __delivery\DPMessengerService.WebService /y /s
C:\Program Files\7-Zip\7z.exe a -tzip __delivery\dpmessenger.zip __delivery\*