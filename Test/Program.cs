﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            string cmd = "";
            do
            {
                cmd = Console.ReadLine();

                if (cmd == "query") Query();
                if (cmd == "dropbox") SendDropbox();
                if (cmd == "fax") SendFax();
                if (cmd == "call") SendCall();
                if (cmd == "sms") SendSMS();
                if (cmd == "sms2") SendSMS2();
                if (cmd == "wh") SendWhatsapp();
                if (cmd == "wh2") SendWhatsapp2();
                if (cmd == "app") SendApp();
                if (cmd == "mail") Send(true, false, true);
                if (cmd == "mail-s") Send(false, false,false);
                if (cmd == "mail-s2") Send(false, true,false);
                if (cmd == "p") Process();
                if (cmd == "c") Cancel();
                if (cmd == "conf") ConfigTest();
                if (cmd == "clear") Console.Clear();


            } while (cmd != "exit");

            


        }

        private static void Query()
        {

            // quand tu créé un message, un nouveau paramètre optionnel est dispo CustomItemId.
            DPMessenger.NewMessage msg = new DPMessenger.NewMessage();
            msg.Init("ALMA", "OCVS", "UserID", "Notification", "TON N° Intervention");
            // ...


            // EXEMPLE 1: 
            // pour requeter l'historique ou l'encours des messages postés,
            // les propriétés de filtrage FilterXXX sont toutes optionnelles
            DPMessenger.Query.MessageStatusQuerier q = new DPMessenger.Query.MessageStatusQuerier();
            q.FilterApplicationId = "GesMobile";
            //q.FilterMessageType = "Notification";
            //q.FilterCustomId = "TON N° Intervention";
            //q.FilterToUserId = "5";
            var messages = q.Get();
            foreach (var item in messages)
            {
                //item.ScheduleDate
                //item.SendDate
                //item.AckDate
                //item.ToApplicationUser
                //item.AnswerDate
                //item.AnswerValue
            }


            // EXEMPLE 2:
            // enrichir la réponse avec des attributs de ton application
            q = new DPMessenger.Query.MessageStatusQuerier();
            q.FilterApplicationId = "GesMobile";
            //q.FilterMessageType = "Notification";
            //q.FilterCustomId = "TON N° Intervention";
            //q.FilterToUserId = "5";

            var userInfo = q.GetDistinctUsers(); // renvoi un tableau distinct de tous les users que tu vas retrouver en suite si tu Get() les messages
            foreach (var user in userInfo)
            {
                string tonUserId = user.ToApplicationUser; // ici tu retrouver le user id de ton app

                user.CustomFirstName = "Jean-" + tonUserId; // tu peux alors renseigner toutes les propriétés CustomXXX que tu veux
                user.CustomLastName = "Jean-" + tonUserId;
            }
            q.SetUsersInfo(userInfo); // donne les infos user aux requeteur


            messages = q.Get(); // tu peux faire ton get messages comme tout l'heure
            foreach (var item in messages)
            {
                //item.ScheduleDate
                //item.SendDate
                //item.AckDate
                //item.ToApplicationUser
                //item.AnswerDate
                //item.AnswerValue

                //item.CustomFirstName; // cette fois ci tu retrouve en plus tes attributs custom
                //item.CustomLastName;
            }

        }

        private static void SendDropbox()
        {
            DPMessenger.DpPostMan service = new DPMessenger.DpPostMan();

            DPMessenger.NewMessage message = new DPMessenger.NewMessage();
            message.Init("TestApp", "TestOrg", "TestUsr", "TestMessage");

            message.Protocol = DPMessenger.MessageProtocol.DROPBOX;
            message.AddAttachement("C:\\Temp\\demo.pdf");

            service.SendNow(message);
        }

        static void SendApp()
        {
            DPMessenger.DpPostMan service = new DPMessenger.DpPostMan();

            DPMessenger.NewMessage message = new DPMessenger.NewMessage();
            message.Init("TestApp", "TestOrg", "TestUsr", "TestMessage");


            message.SetMobileAppNotif("Lapinou", "Un petit lapin");
            message.AddTextAttachment("carotte", "42");
            message.AddTextAttachment("oreilles", "2");

            service.SendNow(message);
        }


        static void ConfigTest()
        {

            string host = "http://6eed76e3.ngrok.io/DpMessengerWebServiceTest/";

            // GLOBAL CONFIGURATION
            DPMessenger.Configuration.Configuration config = new DPMessenger.Configuration.Configuration();
            config.SmtpConfiguration = new DPMessenger.Configuration.SmtpConfiguration();
            config.SmtpConfiguration.AutoBalance = true;
            config.SmtpConfiguration.TimeoutSeconds = 20;
            config.SmtpConfiguration.Servers = new List<DPMessenger.Configuration.SmtpServer>();
            config.SmtpConfiguration.Servers.Add(new DPMessenger.Configuration.SmtpServer
            {
                SmtpHost = "neptun.kreativmedia.ch",
                SmtpPort = 587,
                SmtpLogin = "no-reply@gesmobile.ch",
                SmtpPassword = "8^f25bGj",
                SmtpEnableSSL = true
            });
            config.SmtpConfiguration.Servers.Add(new DPMessenger.Configuration.SmtpServer
            {
                SmtpHost = "neptun.kreativmedia.ch",
                SmtpPort = 587,
                SmtpLogin = "no-reply@gesmobile.ch",
                SmtpPassword = "8^f25bGj",
                SmtpEnableSSL = true
            });
            config.ServiceConfiguration = new DPMessenger.Configuration.ServiceConfiguration();
            config.ServiceConfiguration.BatchNormalSize = 10;
            config.ServiceConfiguration.BatchErrorSize = 3;
            config.ServiceConfiguration.ErrorRetriesLimit = 4;
            config.ServiceConfiguration.RandomizeOrder = true;
            config.EmailAcknowledgement = new DPMessenger.Configuration.EmailAcknowledgement();
            config.EmailAcknowledgement.ImageTrackerUrl = host + "MarkAsRead.ashx?q={ID}";
            config.EmailAcknowledgement.UnsubscribeUrl = host + "Unsubscribe.aspx";
            config.EmailAcknowledgement.ReplyByClickUrl = host + "Reply.aspx?q={ID}&v={VALUE}";

            config.TwilioSettings = new DPMessenger.Configuration.TwilioSettings();
            config.TwilioSettings.AccountSid = "AC9f3aab235d49a3408ec987c7d90acf43";
            config.TwilioSettings.AuthToken = "4db9a2a05c836db0b2a8e9c60e77ae3d";
            config.TwilioSettings.SMSSenderNumber = "+41798073483";
            config.TwilioSettings.WhatsappSenderNumber = "+14155238886";
            config.TwilioSettings.FaxSenderNumber = "+41445051970";
            config.TwilioSettings.FaxDeliveryUrl = host + "Fax.ashx?q={ID}";
            config.TwilioSettings.VoiceSenderNumber = "+41445051970";
            config.TwilioSettings.VoiceDeliveryUrl = host + "Call.ashx?q={ID}";
            config.TwilioSettings.SMSStatusCallBackUrl = host + "SMSStatus.ashx?q={ID}";
            config.TwilioSettings.WhatsappSandboxNumber = "+14155238886";
            config.TwilioSettings.WhatsappSandboxCode = "join fun-apartment";
            config.TwilioSettings.WhatsappStatusCallBackUrl = host + "WhatsAppStatus.ashx?q={ID}";


            config.MobileAttachments = new DPMessenger.Configuration.MobileAttachmentsSettings();
            config.MobileAttachments.AttachmentsUrl = host + "Attachments.ashx?q={ID}";
            config.MobileAttachments.AttachmentsAvailableHours = 5;
            config.MobileAttachments.IPLockAfterErrors = 5;
            config.MobileAttachments.IPLockInMinutes = 15;

            config.InstantReplySettings = new DPMessenger.Configuration.InstantReplySettings();
            config.InstantReplySettings.TextMessageDelayInMinutes = 15;
                

            
            config.SpeachSettings = new DPMessenger.Configuration.SpeachSettings();
            config.SpeachSettings.SoundOpenerMp3Url = host + "assets/call/opener.mp3";
            config.SpeachSettings.Thanks.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Merci" });
            config.SpeachSettings.Thanks.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Thanks" });
            config.SpeachSettings.RepliesClick.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Merci d'indiquer votre choix cliquant sur le lien correspondant." });
            config.SpeachSettings.RepliesClick.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Pleaser, click the link corresponding to your choice." });
            config.SpeachSettings.RepliesMenu.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Merci d'indiquer votre choix en tapant le numéro souhaité en terminant par la touche dièze" });
            config.SpeachSettings.RepliesMenu.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Please, enter the number corresponding to your choice, followd by the pound sign" });
            config.SpeachSettings.RepliesMessage.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Merci d'indiquer votre choix en répondant à ce message par le numéro correspondant:" });
            config.SpeachSettings.RepliesMessage.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Please, reply this message by the number corresponding to your choice:" });
            config.SpeachSettings.ReplyConfirmed.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Choix confirmé: " });
            config.SpeachSettings.ReplyConfirmed.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Choice confirmed: " });
            config.SpeachSettings.ReplyNotFound.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Choix impossible." });
            config.SpeachSettings.ReplyNotFound.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Impossible choice." });
            config.SpeachSettings.WhatsappInvitation.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Notre application souhaite vous envoyer des notifications Whatsapp. Pour accepter, merci d'envoyer par whatsapp: \nMessage:\n$CODE$\nAu numéro:\n$NUMBER$\n\nOu cliquez sur le lien suivant pour créer le message et envoyez-le:\n$LINK$" });
            config.SpeachSettings.WhatsappInvitation.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Our application want to send you notifications via Whatsapp. To accept, thanks to send a whatsapp: \nMessage:\n$CODE$\nTo number:\n$NUMBER$\n\nOr click on the following link to create the message and send it:\n$LINK$" });
            config.SpeachSettings.ReplyBefore.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Réponse possible avant $DATE$." });
            config.SpeachSettings.ReplyBefore.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Answer possible before $DATE$." });

            DPMessenger.Configuration.Configuration.Save(config);


            // FIREBASE CONFIGURATION (GesMobile)

            // Register the APP firebase token (ex: GesMobile)
            string json = "{";
            json += "\n\"type\": \"service_account\",";
            json += "\n\"project_id\": \"gesmobile-8ac16\",";
            json += "\n\"private_key_id\": \"050dce7bf01eaed941a3c6f9df0aa046d0dccd08\",";
            json += "\n\"private_key\": \"-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDB9sRBF+p8EW0d\nWkr0BvrCTTrM3wvRitjhYx/FrNlT14zlwvl6vlfh9QFDsnKnpGOAgjVCYX7ySyGn\nHoKKBlIQJBkx2IDIovUATZC5brv/JbeHj3oC1vHbvb32uMepGBo07Pt/hk8xSD+s\n0dthRBmXqsF5dRGPA/JFvexS1LbieZLfplckORb4B8vALYMrnq7kg1GW5SNqBlUc\nBDESQwBm4r9Qv5v2JyeN8GJokgKU25ZGQexkvQnsjuSecYdx02zqbxN1DGFGJw0q\noYmWSciDvXsMwocV5xXZ3c7brcIDox2TGD39AsfCnCVTZQM0M5AIkw54fHn6c9Ln\nD3V+CyJpAgMBAAECggEABfA+sPmA+dx0uUXW7qK6EbHYn4Yir/8uxbPJuL0iiEEc\ntLoj9kCWoznjmIWBoiVT0I7r/4TJpLRtKtcxyvUHlSvtw4tr4fivRJubVzoPMnAe\nrt1TMHwROpeUFdYxcrlksAMjbhOYIBukq29fZpp1qCGXu9a8O2M1CzKV5jFSChv+\nAPOD1fEB/oldDYd4TYL+08Wu0k8ixkWy8sMnWrRii7in5hyBWxokpmoUG1cbYXqe\nvYQpAcchdyV7b+fnw1dy3UliC1BEiI2pClR6+5/m1jjvKIbz2XagQbNNcn8jBul7\n/49MpofIfYbge/XlCpF5pyAQFVfdCLq8A+SFlIUgmwKBgQD+6mvNzayMFh8P1z/3\ndrI8bQSgWJ0dCQf9/ABZDCLyixY9G+XeEsmQftTDyEZxlhr5NFsTGVgxEvg2Jj3f\nEhEMJPhu1DesMUg3Jw+jx1F6G+bPJl4PPuCleskL98NgHNahiSxNvnmEUfkCMmfj\n2ikzDcpfvI1vvkoDMj41PpBP+wKBgQDCyfmPFcFvUYAqmpZtJwK7OFa/BgWK0v6R\nPsAEcRMzKetOHha7Up7BBvogMw4Kw64mRUR+VYT3sLRFSingOZWRu+1BV35JrXAU\nd9x+xRjIGzBhE3rPsXRTXi4yG0bg+GdyyH8EfP9nonns/VgXMORg27B6f/m1gam/\njPE27cx16wKBgFQnCS2onzSjvN9a3MgRzCc8ihFxD3TdXWEQRwEei47sMYw84Hae\nVyI3bH+xZufqeL8Z9JLqg/23WG9v0m8IpM4Qil7z+ekDX+atI3GaxB/+hASQku6P\ngFPApAwqX7oj6wrqoNCNd8oh0YVTJI7In182Rz+xnDNuazqA5J5pJ/TvAoGBAL9F\n2br5YALRgwtS1VfFsNbrEbNuVE7ntXYmqiMybeoVxavlu+ihzqY5FbA8shM752xJ\nJGLx0ynt6HcJxmkZ4pdCdE1vfuEWYDKZKHmLTTRiDnWkzTnZRSMAHlixlD4hbmO2\nIZPksl+vmg5vD+tl5Tx/PaJsvGXRN0CTQ+HuTJGVAoGAGCAgDumXPt2EqXxMR8h+\nok8S3EdXM+nQD/DjNkAB/CN3RUUaBOILbLP85odZbWrvxK+agTz7mzY0CNAgHxqx\noPpTUJDzA2MIDfrxrH9FmaW2DFTLPz3M22TWAuGty4eZfjQQDTVZd3mHeSewvwaQ\naHUnI3o8xmTow6+vyop+kps=\n-----END PRIVATE KEY-----\n\",";
            json += "\n\"client_email\": \"firebase-adminsdk-1ohty@gesmobile-8ac16.iam.gserviceaccount.com\",";
            json += "\n\"client_id\": \"115923442327168658508\",";
            json += "\n\"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",";
            json += "\n\"token_uri\": \"https://oauth2.googleapis.com/token\",";
            json += "\n\"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",";
            json += "\n\"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-1ohty%40gesmobile-8ac16.iam.gserviceaccount.com\"";
            json += "\n}";
            string url = "https://fcm.googleapis.com/v1/projects/gesmobile-8ac16/messages:send";
            DPMessenger.Configuration.Configuration.SaveFirebaseAppTokenAndUrl("TestApp", json, url);


            // Register the USER girebase token (ex: smallet@digitalpencorp.ch at GesMobile)
            string usertoken = "ejEU7F-KM6E:APA91bEajPGAuFqrZhwAf9djETm6fXtTN4W3fTmhaqvhcKPzfNYL2__IPtVAfj9qqmmv1cXkIPJvsKg_DlAy05STS6TeFLBadxsEjGTAwjOarW97l6yFBqiOEVA9nQaQnLwWXBkAYIp9";
            DPMessenger.Configuration.Configuration.SaveFirebaseUserToken("TestApp", "TestUsr", usertoken);


        }

        static void SendWhatsapp()
        {
            DPMessenger.DpPostMan service = new DPMessenger.DpPostMan();

            DPMessenger.NewMessage message = new DPMessenger.NewMessage();
            message.Init("TestApp", "TestOrg", "TestUsr", "TestMessage");
            message.SetWhatsapp("+41789071134", "Test de Whatsapp par DPCSolutions.com");
            //message.AddAttachement(@"c:\temp\demo.pdf");
            //message.AddAttachement(@"c:\temp\kangaroo.jpg");

            service.SendNow(message);
        }

        static void SendFax()
        {
            DPMessenger.DpPostMan service = new DPMessenger.DpPostMan();

            DPMessenger.NewMessage message = new DPMessenger.NewMessage();
            message.Init("TestApp", "TestOrg", "TestUsr", "TestMessage");
            //message.SetFaxFromPDF("+41 21 811 31 00", "C:\\Temp\\fax.pdf");
            message.SetFaxFromText("+41 21 811 31 00", "My title", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ipsum lorem, suscipit sollicitudin tortor sit amet, feugiat molestie ligula. Pellentesque eleifend augue quis ex maximus aliquet. Donec eu turpis et lacus rhoncus cursus at vel justo. Ut mattis tristique tellus. In faucibus, tellus ac semper egestas, odio augue facilisis diam, a tempor justo nulla eget mauris. Vivamus sed blandit purus. Nunc aliquet suscipit risus, ac facilisis quam fringilla sed. Phasellus ut porttitor nisi, sit amet aliquam nibh. Fusce et posuere nulla. Nullam lacinia sollicitudin tortor, id vulputate ante bibendum vitae."
                + "\n\n"
                + "Aenean blandit, magna sit amet eleifend rutrum, nisl neque tempus risus, vel fermentum massa lectus eget nibh.Nunc mollis porta enim at fringilla.Pellentesque sed pretium lectus.Nulla imperdiet aliquam eros, ac fermentum mauris varius vitae.Pellentesque convallis ligula sed rhoncus luctus.Donec scelerisque risus maximus nisi gravida egestas.Vestibulum egestas bibendum augue mollis dignissim.Ut blandit nisi nec maximus pellentesque.Donec enim dolor, ullamcorper sed est non, vehicula ornare dui.Maecenas eget vehicula dolor, sed commodo odio.Phasellus rutrum diam purus, fringilla gravida ex dapibus non.Aenean mattis vulputate turpis, rhoncus dignissim tortor venenatis scelerisque.Vestibulum luctus posuere ante nec lacinia.Suspendisse potenti."
                + "\n\n"
                + "Mauris eros ante, mollis vitae vestibulum vitae, faucibus quis sem.Quisque sed interdum velit.Nullam ullamcorper laoreet ante, sed rhoncus massa sodales posuere.Nulla et nisl eu neque consectetur porttitor quis ac massa.Curabitur malesuada dapibus lacus, efficitur fermentum augue aliquam vitae.Nullam sodales tempor ultricies.Nunc eu nulla eu nibh sollicitudin interdum.Mauris mattis, tellus et vestibulum condimentum, nunc metus hendrerit elit, quis viverra velit purus nec nisl.Duis ultrices neque semper leo dapibus, non porttitor orci eleifend.Suspendisse sit amet magna ac mauris pharetra vestibulum.");
            service.SendNow(message);
        }
        static void SendSMS2()
        {
            DPMessenger.DpPostMan service = new DPMessenger.DpPostMan();

            DPMessenger.NewMessage message = new DPMessenger.NewMessage();
            message.Init("TestApp", "TestOrg", "TestUsr", "TestMessage");
            message.SetSMS("+41789071134", "Voulez vous accepter le render-vous du 11 avril?",
                "fr-FR",
                new List<DPMessenger.Answers.Answer>()
            {
                new DPMessenger.Answers.Answer
                {
                     Number=1,
                      Value="Yes",
                       Label= new List<DPMessenger.Configuration.VoiceItem>{
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-FR", Text="Oui, accepter."  },
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-CA", Text="Oui, accepter tabernacle."  }
                           }
                },
                new DPMessenger.Answers.Answer
                {
                     Number=2,
                      Value="No",
                       Label= new List<DPMessenger.Configuration.VoiceItem>{
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-FR", Text="Non, refuse."  },
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-CA", Text="Refuse moi ça tout de suite."  }
                           }
                }
            });
            //message.AddAttachement(@"c:\temp\demo.pdf");
            message.AddAttachement(@"c:\temp\kangaroo.jpg");

            service.SendNow(message);
        }

        static void SendWhatsapp2()
        {
            DPMessenger.DpPostMan service = new DPMessenger.DpPostMan();

            DPMessenger.NewMessage message = new DPMessenger.NewMessage();
            message.Init("TestApp", "TestOrg", "TestUsr", "TestMessage");
            message.SetWhatsapp("+41789071134", "Voulez vous accepter le render-vous du 11 avril?",
                "fr-FR",
                new List<DPMessenger.Answers.Answer>()
            {
                new DPMessenger.Answers.Answer
                {
                     Number=1,
                      Value="Yes",
                       Label= new List<DPMessenger.Configuration.VoiceItem>{
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-FR", Text="Oui, accepter."  },
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-CA", Text="Oui, accepter tabernacle."  }
                           }
                },
                new DPMessenger.Answers.Answer
                {
                     Number=2,
                      Value="No",
                       Label= new List<DPMessenger.Configuration.VoiceItem>{
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-FR", Text="Non, refuse."  },
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-CA", Text="Refuse moi ça tout de suite."  }
                           }
                }
            });
            message.AddAttachement(@"C:\DPMessenger\DpMessengerWebServiceTest\assets\call\opener.mp3");
            message.AddAttachement(@"c:\temp\demo.pdf");
            message.AddAttachement(@"c:\temp\kangaroo.jpg");
            message.AddAttachement(@"c:\temp\rabbit.jpg");

            service.SendNow(message);
        }
        static void SendCall()
        {
            DPMessenger.DpPostMan service = new DPMessenger.DpPostMan();

            DPMessenger.NewMessage message = new DPMessenger.NewMessage();
            message.Init("TestApp", "TestOrg", "TestUsr", "TestMessage");
            message.SetCall("+41 78 907 11 34", 
                "Voulez vous accepter le render-vous du 11 avril?", 
                "fr-FR", 
                new List<DPMessenger.Answers.Answer>()
            {
                new DPMessenger.Answers.Answer
                {
                     Number=1,
                      Value="Yes",
                       Label= new List<DPMessenger.Configuration.VoiceItem>{
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-FR", Text="Oui, accepter."  },
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-CA", Text="Oui, accepter tabernacle."  }
                           }
                },
                new DPMessenger.Answers.Answer
                {
                     Number=2,
                      Value="No",
                       Label= new List<DPMessenger.Configuration.VoiceItem>{
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-FR", Text="Non, refuse."  },
                           new DPMessenger.Configuration.VoiceItem{ Language="fr-CA", Text="Refuse moi ça tout de suite."  }
                           }
                }
            });

            service.SendNow(message);
        }

        static void SendSMS()
        {
            DPMessenger.DpPostMan service = new DPMessenger.DpPostMan();

            DPMessenger.NewMessage message = new DPMessenger.NewMessage();
            

            message.Init("TestApp", "TestOrg", "TestUsr", "TestMessage");
            message.SetSMS("+41789071134", "Test de SMS par DPCSolutions.com");
            //message.SetSMS("+‭41764396980‬", "Des alertes par SMS? C'est possible avec GesMobile, même si tu t'appelles David... Même avec des pièces jointes. Vous pouvez aussi répondre 1 pour 'LAPIN' ou 2 pour 'POULPE'.");
            message.AddAttachement(@"c:\temp\demo.pdf");
            message.AddAttachement(@"c:\temp\kangaroo.jpg");

            service.SendNow(message);
            
        }

        static void Send(bool now, bool nevertwice, bool choices)
        {
            DPMessenger.DpPostMan service = new DPMessenger.DpPostMan();

            DPMessenger.NewMessage message = new DPMessenger.NewMessage();
            message.Init("TestApp", "TestOrg", "TestUsr", "TestMessage");

            if (choices)
            {
                message.SetEmail("seubiracing@gmail.com", "smallet@digitalpencorp.ch", null, "Test avec réponse", "Voulez vous accepter le RDV blabla ?", "fr-FR",
                    new List<DPMessenger.Answers.Answer>()
                    {
                        new DPMessenger.Answers.Answer
                        {
                             Number=1,
                              Value="Yes",
                               Label= new List<DPMessenger.Configuration.VoiceItem>{
                                   new DPMessenger.Configuration.VoiceItem{ Language="fr-FR", Text="Oui, accepter."  },
                                   new DPMessenger.Configuration.VoiceItem{ Language="fr-CA", Text="Oui, accepter tabernacle."  }
                                   }
                        },
                        new DPMessenger.Answers.Answer
                        {
                             Number=2,
                              Value="No",
                               Label= new List<DPMessenger.Configuration.VoiceItem>{
                                   new DPMessenger.Configuration.VoiceItem{ Language="fr-FR", Text="Non, refuse."  },
                                   new DPMessenger.Configuration.VoiceItem{ Language="fr-CA", Text="Refuse moi ça tout de suite."  }
                                   }
                        }
                    });
            }
            else
            {
                message.SetEmail("seubiracing@gmail.com", "smallet@digitalpencorp.ch", /*"simracersclub@gmail.com"*/null, "Test email 2", "Here all the message...");
            }
            message.AddAttachement(@"c:\temp\demo.pdf");

            if (nevertwice)
            {
                //message.AllowMultiple = false;
                message.AllowMultipleFrequency = TimeSpan.FromMinutes(2);
            }

            if (now)
            {
                service.SendNow(message);
            }
            else
            {
                service.Enqueue(message, DateTime.Now.AddMinutes(2));
            }
        }

        static void Cancel()
        {
            DPMessenger.DpPostMan service = new DPMessenger.DpPostMan();

            service.CancelMessages("TestApp", "TestOrg", "TestUsr", "TestMessage");
        }

        static void Process()
        {
            DPMessenger.Service.DpMessengerServiceImplementation service = new DPMessenger.Service.DpMessengerServiceImplementation();

            service.DoOnce();
        }
    }
}
