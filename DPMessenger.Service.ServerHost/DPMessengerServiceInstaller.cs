﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace DPMessenger.Service.ServerHost
{
    [RunInstaller(true)]
    public partial class DPMessengerServiceInstaller : System.Configuration.Install.Installer
    {
        public DPMessengerServiceInstaller()
        {

            ServiceInstaller monServiceInstaller = new ServiceInstaller();
            ServiceProcessInstaller monProcessInstaller = new ServiceProcessInstaller();
            monServiceInstaller.ServiceName = "DPMessenger.Service.ServerHost";
            monServiceInstaller.DisplayName = "DPMessenger.Service.ServerHost";
            monServiceInstaller.StartType = ServiceStartMode.Automatic;
            this.Installers.Add(monServiceInstaller);
            monProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.Installers.Add(monProcessInstaller);
        }

        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand)]
        public override void Uninstall(IDictionary savedState)
        {
            try
            {
                //System.Windows.Forms.MessageBox.Show("Uninstall PenConnector");
                base.Uninstall(savedState);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                System.Windows.Forms.MessageBox.Show(ex.StackTrace);
            }
        }

        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand)]
        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            try
            {
                //System.Windows.Forms.MessageBox.Show("OnBeforeUninstall PenConnector");
                base.OnBeforeUninstall(savedState);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                System.Windows.Forms.MessageBox.Show(ex.StackTrace);
            }
        }
    }
}
