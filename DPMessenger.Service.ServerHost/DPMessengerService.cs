﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.Service.ServerHost
{
    public partial class DPMessengerService : ServiceBase
    {
        public DPMessengerService()
        {
            InitializeComponent();
        }

        DPMessenger.Service.DpMessengerServiceImplementation service = null;

        protected override void OnStart(string[] args)
        {
            service = new DpMessengerServiceImplementation();
            service.RunThreadedLoop();
        }

        protected override void OnStop()
        {
            service.Stop(false);
        }
    }
}
