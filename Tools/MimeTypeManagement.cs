﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.Tools
{
    public class MimeTypeManagement
    {
        public static string ToMime(AttachmentType type)
        {
            switch (type)
            {
                case AttachmentType.PDF:
                    return "application/pdf";
                    
                case AttachmentType.DOC:
                    return "application/msword";
                    
                case AttachmentType.DOCX:
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    
                case AttachmentType.XLS:
                    return "application/vnd.ms-excel";
                    
                case AttachmentType.XLSX:
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    
                case AttachmentType.ZIP:
                    return "application/zip";
                    
                case AttachmentType.TXT:
                    return "text/plain";
                    
                case AttachmentType.RTF:
                    return "application/rtf";
                    
                case AttachmentType.WAV:
                    return "audio/x-wav";
                    
                case AttachmentType.MP3:
                    return "audio/mpeg";
                    
                case AttachmentType.JPG:
                    return "image/jpeg";
                    
                case AttachmentType.PNG:
                    return "image/png";
                    
                case AttachmentType.GIF:
                    return "image/gif";
                    
                case AttachmentType.BMP:
                    return "image/bmp";
                    

                default:
                    break;
            }
            return "application/octet-stream";
        }
    }
}
