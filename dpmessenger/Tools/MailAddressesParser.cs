﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DPMessenger.Tools
{
    public class MailAddressesParser
    {
        public List<string> Addresses { get; set; }
        public string AddressesAsString { get; set; }

        public void Parse(string str)
        {
            if (!String.IsNullOrWhiteSpace(str))
            {
                Regex emailRegex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
                    RegexOptions.IgnoreCase);
                MatchCollection emailMatches = emailRegex.Matches(str);

                StringBuilder sb = new StringBuilder();

                Addresses = new List<string>();
                foreach (Match emailMatch in emailMatches)
                {
                    Addresses.Add(emailMatch.Value);
                }

                AddressesAsString = "";
                for (int i = 0; i < Addresses.Count; i++)
                {
                    AddressesAsString += Addresses[i];
                    if (i < Addresses.Count - 1) AddressesAsString += "; ";
                }
            }
            else
            {
                Addresses = new List<string>();
                AddressesAsString = null;
            }
        }
    }
}
