﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPMessenger.Model;

namespace DPMessenger.Service
{
    public class DpMessengerServiceImplementation
    {

        Configuration.Configuration _config;

        public string ProtocolFilter { get; set; }
        


        Dictionary<string, SendingProtocols.IProtocolSender> _implementations;


        public DpMessengerServiceImplementation()
        {
            ProtocolFilter = "*";

            
            _config = Configuration.Configuration.Read();

            LoopPause = TimeSpan.FromSeconds(2);



        }

        Model.DpMessengerEntities db;

        private readonly object _locker = new object();
        public void DoOnce()
        {
            lock (_locker)
            {
                List<Model.dpm_message> todo = new List<Model.dpm_message>();

                db = new DpMessengerEntities();

                // normal queue
                Log.LogEvent("Core", true, "Find " + _config.ServiceConfiguration.BatchNormalSize + " new messages to send.");
                var q = (from r in db.dpm_message
                         where r.SendDate == null && r.SendErrorRetries == 0
                         && r.ScheduleDate <= DateTime.Now
                         && (r.Canceled == null || r.Canceled == 0)
                         select r);
                if (!String.IsNullOrWhiteSpace(ProtocolFilter) && ProtocolFilter != "*")
                {
                    q = (from r in q where r.Protocol == ProtocolFilter select r);
                }
                var res = q.Take(_config.ServiceConfiguration.BatchNormalSize).ToList();
                todo.AddRange(res);
                Log.LogEvent("Core", true, res.Count + " new messages to send found.");

                // -->

                // error queue
                DateTime dtRetry = DateTime.Now.Subtract(TimeSpan.FromMinutes(1));
                Log.LogEvent("Core", true, "Find " + _config.ServiceConfiguration.BatchNormalSize + " error messages to retry.");
                q = (from r in db.dpm_message
                     where r.SendDate == null && r.SendErrorRetries > 0 && r.SendErrorRetries <= _config.ServiceConfiguration.ErrorRetriesLimit
                     && r.ScheduleDate <= dtRetry
                     && (r.Canceled == null || r.Canceled == 0)
                     select r);
                if (!String.IsNullOrWhiteSpace(ProtocolFilter) && ProtocolFilter != "*")
                {
                    q = (from r in q where r.Protocol == ProtocolFilter select r);
                }
                res = q.Take(_config.ServiceConfiguration.BatchErrorSize).ToList();
                todo.AddRange(res);
                Log.LogEvent("Core", true, res.Count + " error messages to retry found.");
                // -->


                if (_config.ServiceConfiguration.RandomizeOrder && todo.Count > 1)
                {
                    Random rdm = new Random();
                    todo = todo.OrderBy(x => rdm.Next(1000)).ToList();
                    Log.LogEvent("Core", true, "Randomize queue order by shuffling the list.");
                }


                List<string> mails = new List<string>();
                
                foreach (var m in todo)
                {
                    if (!mails.Contains(m.ID))
                    {
                        mails.Add(m.ID);
                        TreadMessage(m);
                        db.SaveChanges();
                    }
                }

                Log.LogEvent("Core", true, "End of actions.");
            }
            
        }

        public void RunThreadedLoop()
        {
            if(LoopThread != null)
            {
                Stop(true);
            }
            LoopThread = new System.Threading.Thread(new System.Threading.ThreadStart(RunLoop));
            LoopThread.Start();
        }

        public bool LoopStarted { get; private set; }
        public TimeSpan LoopPause { get; set; }
        public System.Threading.Thread LoopThread { get; set; }

        public void RunLoop()
        {
            LoopStarted = true;
            while (LoopStarted)
            {
                try
                {
                    DoOnce();
                }
                catch
                {

                }
      
                System.Threading.Thread.Sleep(Convert.ToInt32(LoopPause.TotalMilliseconds));
            }
        }

        public void Stop(bool forceKillThread)
        {
            LoopStarted = false;
            if (forceKillThread && LoopThread != null)
            {
                try
                {
                    LoopThread.Abort();
                }
                catch { }
            }
        }

        private void LoadImplementations()
        {
            if (_implementations == null)
            {
                _implementations = new Dictionary<string, SendingProtocols.IProtocolSender>();
                foreach (Type t in typeof(SendingProtocols.IProtocolSender).Assembly.GetTypes())
                {
                    if ((from r in t.GetInterfaces() where r.Name == "IProtocolSender" select r).Count() > 0)
                    {
                        var p = Activator.CreateInstance(t) as SendingProtocols.IProtocolSender;
                        if (p != null)
                        {
                            if (!String.IsNullOrWhiteSpace(ProtocolFilter) && ProtocolFilter != "*")
                            {
                                if(p.Protocol.Equals(ProtocolFilter)) _implementations.Add(p.Protocol, p);
                            }
                            else
                            {
                                _implementations.Add(p.Protocol, p);
                            }
                        }
                    }
                }
            }
        }

        private void TreadMessage(dpm_message m)
        {
            LoadImplementations();

            Log.LogEvent("Core", true, "Treat message " + m.ID);
            if (_implementations.ContainsKey(m.Protocol))
            {
                var imp = _implementations[m.Protocol];
                imp.Configuration = _config;
                imp.Send(m);
                m.SendServer = imp.Server;
                m.SendingDuration = imp.SendingDuration;
                if (imp.Success)
                {
                    MarkMessageAsSent(m, imp.Server, imp.SendingDuration);
                }
                else
                {
                    MarkMessageAsError(m, imp.ErrorMessage);
                }
            }
            else
            {
                Log.LogEvent("Core", false, "No implementation of the protocol " + m.Protocol);
                MarkMessageAsError(m, "No implementation of the protocol " + m.Protocol);
            }

        }

        private void MarkMessageAsSent(dpm_message m, string server, double sendingduration)
        {
            Log.LogEvent("CoreSend", true, "Message " + m.ID + " was sent in " + sendingduration + " sec via server " + server);

            m.SendDate = DateTime.Now;
            m.dpm_log.Add(new dpm_log
            {
                Date = m.SendDate.Value,
                IsError = false,
                Message = "Message was sent in " + sendingduration + " sec. via server " + server
            });
        }

        private void MarkMessageAsError(dpm_message m, string error)
        {
            Log.LogEvent("Core", false, "Message " + m.ID + " can not be sent. Error :  " + error);

            m.SendErrorRetries++;
            m.SendDate = null;
            m.dpm_log.Add(new dpm_log
            {
                Date = DateTime.UtcNow,
                IsError = true,
                Message = error
            });
        }
    }
}
