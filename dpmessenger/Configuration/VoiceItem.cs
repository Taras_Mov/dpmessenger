﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace DPMessenger.Configuration
{
    public class VoiceItem
    {
        [XmlAttribute()]
        public string Language { get; set; }
        [XmlAttribute()]
        public string Text { get; set; }

        /*
        public static string Serialize(List<VoiceItem> o)
        {
            string ret = null;
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializer ser = new XmlSerializer(o.GetType());
                XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                xtw.Indentation = 4;
                ser.Serialize(xtw, o);
                byte[] data = ((MemoryStream)xtw.BaseStream).ToArray();
                ret = Encoding.UTF8.GetString(data);
                ret = ret.Trim();
                int start = ret.IndexOf("<?");
                ret = ret.Substring(start);
            }
            return ret;
        }

        public static List<VoiceItem> Deserialize(string xml)
        {
            
            Stream s = new MemoryStream(Encoding.UTF8.GetBytes(xml));

            Exception e = null;
            XmlSerializer ser = null;
            object o = null;
            try
            {
                ser = new XmlSerializer(typeof(List<VoiceItem>));
                o = ser.Deserialize(s);
            }
            catch (Exception ex)
            {
                e = ex;
            }
            s.Dispose(); // end stream
            if (e != null) throw e;
            return (List<VoiceItem>)o;
        }*/
    }



}
