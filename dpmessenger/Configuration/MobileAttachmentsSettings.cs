﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DPMessenger.Configuration
{
    public class MobileAttachmentsSettings
    {
        [XmlAttribute()]
        public int AttachmentsAvailableHours { get; set; }


        [XmlAttribute()]
        public string AttachmentsUrl { get; set; }

        [XmlAttribute()]
        public int IPLockAfterErrors { get; set; }

        [XmlAttribute()]
        public int IPLockInMinutes { get; set; }
    }
}
