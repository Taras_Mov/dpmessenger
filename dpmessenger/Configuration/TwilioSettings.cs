﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.Configuration
{
    public class TwilioSettings
    {
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string SMSSenderNumber { get; set; }
        public string SMSStatusCallBackUrl { get; set; }
        public string WhatsappStatusCallBackUrl { get; set; }
        public string WhatsappSenderNumber { get; set; }
        public string VoiceSenderNumber { get; set; }

        public string VoiceDeliveryUrl { get; set; }

        public string FaxSenderNumber { get; set; }

        public string FaxDeliveryUrl { get; set; }
        

        public string WhatsappSandboxNumber { get; set; }
        public string WhatsappSandboxCode { get; set; }


    }
}
