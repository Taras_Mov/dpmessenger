﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DPMessenger.Configuration
{
    public class InstantReplySettings
    {
        [XmlAttribute()]
        public int TextMessageDelayInMinutes { get; set; }
    }
}
