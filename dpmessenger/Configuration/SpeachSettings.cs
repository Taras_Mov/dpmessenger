﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.Configuration
{
    public class SpeachSettings
    {
        public List<VoiceItem> RepliesMenu { get; set; }
        public List<VoiceItem> RepliesMessage { get; set; }
        public List<VoiceItem> RepliesClick { get; set; }
        public List<VoiceItem> Thanks { get; set; }
        public List<VoiceItem> ReplyNotFound { get; set; }
        public List<VoiceItem> ReplyConfirmed { get; set; }
        public List<VoiceItem> WhatsappInvitation { get; set; }
        public List<VoiceItem> ReplyBefore { get; set; }
       



        public string SoundOpenerMp3Url { get; set; }

        public SpeachSettings()
        {
            RepliesMenu = new List<VoiceItem>();
            RepliesMessage = new List<VoiceItem>();
            RepliesClick = new List<VoiceItem>();
            Thanks = new List<VoiceItem>();
            ReplyNotFound = new List<VoiceItem>();
            ReplyConfirmed = new List<VoiceItem>();
            WhatsappInvitation = new List<VoiceItem>();
            ReplyBefore = new List<VoiceItem>();
        }
    }
}
