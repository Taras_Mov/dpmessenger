﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;


namespace DPMessenger.Configuration
{
    public class SmtpServer
    {
        [XmlAttribute()]
        public string SmtpHost { get; set; }


        [XmlAttribute()]
        public int SmtpPort { get; set; }


        [XmlAttribute()]
        public string SmtpLogin { get; set; }


        [XmlAttribute()]
        public string SmtpPassword { get; set; }


        [XmlAttribute()]
        public bool SmtpEnableSSL { get; set; }

        [XmlAttribute()]
        public string TrustedSender { get; set; }
    }

    public class SmtpConfiguration
    {
        public List<SmtpServer> Servers { get; set; }



        [XmlAttribute()]
        public bool AutoBalance { get; set; }
        [XmlAttribute()]
        public int TimeoutSeconds { get; set; }
    }
}
