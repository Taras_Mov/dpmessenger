﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DPMessenger.Configuration
{
    public class Configuration
    {
        public SmtpConfiguration SmtpConfiguration { get; set; }
        public ServiceConfiguration ServiceConfiguration { get; set; }

        public EmailAcknowledgement EmailAcknowledgement { get; set; }

        public TwilioSettings TwilioSettings { get; set; }

        public MobileAttachmentsSettings MobileAttachments { get; set; }
        public SpeachSettings SpeachSettings { get; set; }

        public InstantReplySettings InstantReplySettings { get; set; }


        public VoiceItem GetSpeachValue(string name, string language)
        {
            VoiceItem ret = null;

            if (language == null) language = "fr-FR";


            // to do. a rendre générique avec de la reflexion
            List<VoiceItem> items = null;
            /*if(name == "RepliesMenu") items = SpeachSettings.RepliesMenu;
            else if(name == "RepliesMessage") items = SpeachSettings.RepliesMessage;
            else if (name == "ReplyConfirmed") items = SpeachSettings.ReplyConfirmed;
            else if (name == "ReplyNotFound") items = SpeachSettings.ReplyNotFound;
            else if (name == "Thanks") items = SpeachSettings.Thanks;
            else if (name == "WhatsappInvitation") items = SpeachSettings.WhatsappInvitation;*/
            Type t = SpeachSettings.GetType();
            var p = t.GetProperty(name);
            items = (List<VoiceItem>)p.GetValue(SpeachSettings);
            // -->

            if(items != null)
            {
                ret = (from r in items where r.Language == language select r).FirstOrDefault();

                if (ret == null)
                {
                    language = language.ToLower();
                    ret = (from r in items where r.Language.ToLower().StartsWith(language) || language.StartsWith(r.Language.ToLower()) select r).FirstOrDefault();
                    
                }

                if(ret == null)
                {
                    ret = items.FirstOrDefault();
                }
            }

            return ret;
        }

        public static void SaveFirebaseAppTokenAndUrl(string applicationName, string json, string url)
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var s = (from r in db.dmp_setting where r.Name == "FIREBASEAPPAUTH:"+applicationName && r.Type == "FIREBASEAPPAUTH" select r).FirstOrDefault();
            if (s != null)
            {
                s.Value = json;
            }
            else
            {
                var set = new Model.dmp_setting
                {
                    Name = "FIREBASEAPPAUTH:" + applicationName,
                    Type = "FIREBASEAPPAUTH",
                    Value = json
                };

                db.dmp_setting.Add(set);
            }


            var s2 = (from r in db.dmp_setting where r.Name == "FIREBASEAPPURL:" + applicationName && r.Type == "FIREBASEAPPURL" select r).FirstOrDefault();
            if (s2 != null)
            {
                s2.Value = url;
            }
            else
            {
                var set2 = new Model.dmp_setting
                {
                    Name = "FIREBASEAPPURL:" + applicationName,
                    Type = "FIREBASEAPPURL",
                    Value = url
                };

                db.dmp_setting.Add(set2);
            }

            db.SaveChanges();
        }
        public static string GetFirebaseAppToken(string applicationName)
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var s = (from r in db.dmp_setting where r.Name == "FIREBASEAPPAUTH:" + applicationName  && r.Type == "FIREBASEAPPAUTH" select r).FirstOrDefault();
            if (s == null)
            {
                throw new KeyNotFoundException("FIREBASEAPPAUTH not found. No settings for applicationName=" + applicationName);
            }
            return s.Value;
        }

        public static bool IsWhatappInviationSent(string phoneNumber)
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var s = (from r in db.dmp_setting where r.Name == "WHATSAPPINVITATION:" + phoneNumber && r.Type == "WHATSAPPINVITATION" select r).FirstOrDefault();
            if (s == null)
            {
                return false;
            }
            return true;
        }

        public static void SetWhatappInviationSent(string phoneNumber, bool v)
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var s = (from r in db.dmp_setting where r.Name == "WHATSAPPINVITATION:" + phoneNumber && r.Type == "WHATSAPPINVITATION" select r).FirstOrDefault();
            if (s != null)
            {
                if (v)
                {
                    // already ok
                }
                else
                {
                    db.dmp_setting.Remove(s);
                    db.SaveChanges();
                }
            }
            else
            {
                if (v)
                {
                    Model.dmp_setting m = new Model.dmp_setting
                    {
                        Name = "WHATSAPPINVITATION:" + phoneNumber,
                        Type = "WHATSAPPINVITATION",
                        Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                    };
                    db.dmp_setting.Add(m);
                    db.SaveChanges();
                }
                else
                {
                    // already ok
                }
            }
        }

        public static string GetFirebaseAppUrl(string applicationName)
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var s = (from r in db.dmp_setting where r.Name == "FIREBASEAPPURL:" + applicationName && r.Type == "FIREBASEAPPURL" select r).FirstOrDefault();
            if (s == null)
            {
                throw new KeyNotFoundException("FIREBASEAPPURL not found. No settings for applicationName=" + applicationName);
            }
            return s.Value;
        }

        public static void SaveFirebaseUserToken(string applicationName, string applicationUserId, string token)
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var s = (from r in db.dmp_setting where r.Name == "FIREBASEUSRTOKEN:" + applicationName + "/" + applicationUserId && r.Type == "FIREBASEUSRTOKEN" select r).FirstOrDefault();
            if (s != null)
            {
                s.Value = token;
            }
            else
            {
                var set = new Model.dmp_setting
                {
                    Name = "FIREBASEUSRTOKEN:" + applicationName + "/" + applicationUserId,
                    Type = "FIREBASEUSRTOKEN",
                    Value = token
                };

                db.dmp_setting.Add(set);
            }
            db.SaveChanges();
        }

        public static string GetFirebaseUserToken(string applicationName, string applicationUserId)
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var s = (from r in db.dmp_setting where r.Name == "FIREBASEUSRTOKEN:" + applicationName + "/" + applicationUserId && r.Type == "FIREBASEUSRTOKEN" select r).FirstOrDefault();
            if(s == null)
            {
                throw new KeyNotFoundException("FIREBASEUSRTOKEN not found. No settings for applicationName=" + applicationName + " and applicationUserId=" + applicationUserId);
            }
            return s.Value;
        }


        public static void Save(Configuration c)
        {
            string xml = Serialize(c);

            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var s = (from r in db.dmp_setting where r.Name == "XMLSETTINGS" && r.Type == "GLOBAL" select r).FirstOrDefault();
            if(s != null)
            {
                s.Value = xml;
            }
            else
            {
                var set = new Model.dmp_setting
                {
                    Name = "XMLSETTINGS",
                    Type = "GLOBAL",
                    Value = xml
                };

                db.dmp_setting.Add(set);
            }
            db.SaveChanges();
        }

        static string Serialize(object o)
        {
            string ret = null;
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializer ser = new XmlSerializer(o.GetType());
                XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                xtw.Indentation = 4;
                ser.Serialize(xtw, o);
                byte[] data = ((MemoryStream)xtw.BaseStream).ToArray();
                ret = Encoding.UTF8.GetString(data);
                ret = ret.Trim();
                int start = ret.IndexOf("<?");
                ret = ret.Substring(start);
            }
            return ret;
        }

        public static Configuration Read()
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var set = (from r in db.dmp_setting where r.Name == "XMLSETTINGS" && r.Type == "GLOBAL" select r).FirstOrDefault();
            if (set == null) throw new Exception("No 'XMLSETTINGS' found in dpm_settings table");

            Stream s = new MemoryStream(Encoding.UTF8.GetBytes(set.Value.Trim()));

            Exception e = null;
            XmlSerializer ser = null;
            object o = null;
            try
            {
                ser = new XmlSerializer(typeof(Configuration));
                //ser.UnknownAttribute += new XmlAttributeEventHandler(ser_UnknownAttribute);
                //ser.UnknownElement += new XmlElementEventHandler(ser_UnknownElement);
                //ser.UnknownNode += new XmlNodeEventHandler(ser_UnknownNode);
                
                o = ser.Deserialize(s);
                Log.LogEvent("Core", true, "XMLSETTINGS loaded."/* + set.Value*/);
            }
            catch (Exception ex)
            {
                Log.LogEvent("Core", false, "Impossible to deserialize XMLSETTINGS. " + ex.Message);
                e = ex;
            }
            s.Dispose(); // end stream
            if (e != null) throw e;
            return (Configuration)o;
        }

        public static string GetMiniCss()
        {
            return Properties.Resources.minicss;

        }
    }
}
