﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DPMessenger.Configuration
{
    public class ServiceConfiguration
    {
        [XmlAttribute()]
        public int BatchNormalSize { get; set; }

        [XmlAttribute()]
        public int BatchErrorSize { get; set; }

        [XmlAttribute()]
        public int ErrorRetriesLimit { get; set; }

        [XmlAttribute()]
        public bool RandomizeOrder { get; set; }
    }
}
