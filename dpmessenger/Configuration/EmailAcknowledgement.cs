﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DPMessenger.Configuration
{
    public class EmailAcknowledgement
    {
        [XmlAttribute()]
        public string ImageTrackerUrl { get; set; }

        [XmlAttribute()]
        public string UnsubscribeUrl { get; set; }

        [XmlAttribute()]
        public string ReplyByClickUrl { get; set; }
    }
}
