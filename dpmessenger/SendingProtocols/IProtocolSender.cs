﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.SendingProtocols
{
    public interface IProtocolSender
    {
        // configuration
        string Protocol { get; }
        Configuration.Configuration Configuration { get; set; }
        // -->


        // action 
        void Send(Model.dpm_message message);
        // -->

        // result
        bool Success { get; }
        string ErrorMessage { get; }
        double SendingDuration { get;  }
        string Server { get;  }

        // -->
    }
}
