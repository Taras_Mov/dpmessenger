﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPMessenger.Configuration;
using DPMessenger.Model;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
namespace DPMessenger.SendingProtocols
{
    public class WhatsappSender : IProtocolSender
    {
        public string Protocol
        {
            get
            {
                return MessageProtocol.WHATSAPP.ToString();
            }
        }


        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public double SendingDuration { get; set; }
        public string Server { get; set; }

        public Configuration.Configuration Configuration { get; set; }


        private bool Join(dpm_message message)
        {
            var config = DPMessenger.Configuration.Configuration.Read();
            if (!DPMessenger.Configuration.Configuration.IsWhatappInviationSent(message.ToPhoneNumber))
            {
                DpPostMan sender = new DpPostMan();
                NewMessage msg = new NewMessage();
                msg.Init(message.Application, message.ToApplicationOrganization, message.ToApplicationUser, "WHATSAPPINVITATION");

                string lang = message.Language;
                if (lang == null) message.Language = "fr-FR";

                //string link = "https://api.whatsapp.com/send?phone=14155238886&text=join%20fun-apartment";

                string link = "https://api.whatsapp.com/send?phone="
                    + config.TwilioSettings.WhatsappSandboxNumber.Replace(" ", "").Replace("+", "")
                    + "&text="
                    + System.Web.HttpUtility.UrlEncode(config.TwilioSettings.WhatsappSandboxCode);

                string text = "Our application want to send you notifications via Whatsapp. To accept, thanks to send a whatsapp: \nMessage:\n$CODE$\nTo number:\n$NUMBER$\n\nOr click on the following link to create the message and send it:\n$LINK$";
                var customText = config.GetSpeachValue("WhatsappInvitation", lang);
                if (customText != null && !String.IsNullOrWhiteSpace(customText.Text)) text = customText.Text;

                text = text.Replace("$CODE$", config.TwilioSettings.WhatsappSandboxCode);
                text = text.Replace("$NUMBER$", config.TwilioSettings.WhatsappSandboxNumber);
                text = text.Replace("$LINK$", link);

                msg.SetSMS(message.ToPhoneNumber, text);
                sender.SendNow(msg);

                DPMessenger.Configuration.Configuration.SetWhatappInviationSent(message.ToPhoneNumber, true);
                return true;
            }


            return false;
        }


        public void Send(dpm_message message)
        {
            Server = Configuration.TwilioSettings.AccountSid + "@twilio.com";
            if(Join(message))
            {
                // delay the original message
                message.ScheduleDate = message.ScheduleDate.AddMinutes(2);
                Success = false;
                ErrorMessage = "Message was delayed to 2 minutes because SMS invitation was sent first";
                return;


            }


            try
            {

                TwilioClient.Init(Configuration.TwilioSettings.AccountSid, Configuration.TwilioSettings.AuthToken);
            }
            catch
            {
                Success = false;
                ErrorMessage = "Impossible to init TwilioClient with credentials";
            }

            try
            {
                string text = message.Message;

                List<Uri> attachementsFirstImage = new List<Uri>();
                List<Uri> attachementsOthers = new List<Uri>();

                if (message.dpm_attachment.Count > 0
                    && Configuration.MobileAttachments != null
                    && !String.IsNullOrWhiteSpace(Configuration.MobileAttachments.AttachmentsUrl))
                {
                    

                    


                    //bool containsNonImageAttachments = false;
                    foreach (var att in message.dpm_attachment)
                    {
                        string url = Configuration.MobileAttachments.AttachmentsUrl.Replace("{ID}", message.ID);
                        url += "&id=" + att.ID;

                        if (att.MimeType.StartsWith("image/") && attachementsFirstImage.Count == 0)
                        {

                            attachementsFirstImage.Add(new Uri(url));
                        }
                        else
                        {
                            attachementsOthers.Add(new Uri(url));

                        }
                    }


                    
                }

                if (!String.IsNullOrEmpty(message.PossibleAnswers))
                {
                    var answers = Answers.Answer.Deserialize(message.PossibleAnswers);

                    var spRepliesMenu = Configuration.GetSpeachValue("RepliesMessage", message.Language);
                    if (spRepliesMenu != null)
                    {
                        text += "\n\n " + spRepliesMenu.Text;

                    }

                    foreach (var item in answers)
                    {
                        string label = item.GetLabel(message.Language);
                        text += "\n" + item.Number + " : " + label;
                    }

                    int delay = 15;
                    if (Configuration.InstantReplySettings != null && Configuration.InstantReplySettings.TextMessageDelayInMinutes > 0) delay = Configuration.InstantReplySettings.TextMessageDelayInMinutes;
                    DateTime before = DateTime.Now.Add(TimeSpan.FromMinutes(delay));
                    string spReplyBeforeText = "Reply before $DATE$.";
                    var spReplyBefore = Configuration.GetSpeachValue("ReplyBefore", message.Language);
                    if (spReplyBefore != null) spReplyBeforeText = spReplyBefore.Text;
                    spReplyBeforeText = spReplyBeforeText.Replace("$DATE$", before.ToString("HH:mm"));
                    text += "\n\n " + spReplyBeforeText;

                    CloseOtherOpenedWhatsappFor(message.ToPhoneNumber, message.ID);
                }

                foreach (var a in attachementsOthers)
                {
                    var whatt = MessageResource.Create(
                        body: "",
                        from: new Twilio.Types.PhoneNumber("whatsapp:" + Configuration.TwilioSettings.WhatsappSenderNumber),
                        to: new Twilio.Types.PhoneNumber("whatsapp:" + message.ToPhoneNumber),
                        mediaUrl: new List<Uri> { a }
                    );
                }

                var wh = MessageResource.Create(
                    body: text,
                    from: new Twilio.Types.PhoneNumber("whatsapp:"+Configuration.TwilioSettings.WhatsappSenderNumber),
                    to: new Twilio.Types.PhoneNumber("whatsapp:"+message.ToPhoneNumber),
                    mediaUrl: attachementsFirstImage,
                    statusCallback: new Uri(Configuration.TwilioSettings.WhatsappStatusCallBackUrl.Replace("{ID}", message.ID))
                );

                Success = true;
            }
            catch
            {
                Success = false;
                ErrorMessage = "Impossible to send SMS with TwilioClient";
            }


        }

        private void CloseOtherOpenedWhatsappFor(string toPhoneNumber, string beforeMessageId)
        {
            Model.DpMessengerEntities db = new DpMessengerEntities();
            var openedMessagesToClose = (from r in db.dpm_message
                                         where r.ID != beforeMessageId
                                         && r.Protocol == "WHATSAPP"
                                         && r.ToPhoneNumber == toPhoneNumber
                                         && !String.IsNullOrEmpty(r.PossibleAnswers)
                                         && (r.AnswerClosed == null || r.AnswerClosed == 0)
                                         && r.AnswerDate == null
                                         select r).ToList();
            foreach (var m in openedMessagesToClose)
            {
                m.AnswerClosed = 1;
                m.dpm_log.Add(new dpm_log
                {
                    Date = DateTime.Now,
                    IsError = false,
                    Message = "Answers are close because a new SMS was sent to the same number " + toPhoneNumber + " - message: " + beforeMessageId
                });
            }
            db.SaveChanges();
        }
    }
}
