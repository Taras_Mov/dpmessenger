﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using DPMessenger.Model;
using System.IO;
using Dropbox.Api.Files;

namespace DPMessenger.SendingProtocols
{
    public class DropboxSender : IProtocolSender
    {
        public string Protocol
        {
            get
            {
                return MessageProtocol.DROPBOX.ToString();
            }
        }

       

        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public double SendingDuration { get; set; }
        public string Server { get; set; }

        public Configuration.Configuration Configuration { get; set; }

        Dropbox.Api.DropboxClient client;
        public void Send(dpm_message message)
        {

            

            Success = true;
            client = new Dropbox.Api.DropboxClient("y72iwpwqi6pyq0l");

            //get a token here : https://www.dropbox.com/developers/apps/create

            // regarder le code ici : https://temboo.com/csharp/upload-to-dropbox

            foreach (var att in message.dpm_attachment)
            {
               
                UploadAttachment(att);
            }
        }

        private void UploadAttachment(dpm_attachment att)
        {
            using (var mem = new MemoryStream(att.Content))
            {

                var updated = this.client.Files.UploadAsync(
                    "/"+att.FileName,
                    WriteMode.Overwrite.Instance,
                    body: mem).Result;


            }
        }


       
    }
}
