﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPMessenger.Configuration;
using DPMessenger.Model;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Fax.V1;

namespace DPMessenger.SendingProtocols
{
    public class FaxSender : IProtocolSender
    {
        public string Protocol
        {
            get
            {
                return MessageProtocol.FAX.ToString();
            }
        }


        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public double SendingDuration { get; set; }
        public string Server { get; set; }

        public Configuration.Configuration Configuration { get; set; }
        public void Send(dpm_message message)
        {
            Server = Configuration.TwilioSettings.AccountSid + "@twilio.com";



            try
            {

                TwilioClient.Init(Configuration.TwilioSettings.AccountSid, Configuration.TwilioSettings.AuthToken);
            }
            catch
            {
                Success = false;
                ErrorMessage = "Impossible to init TwilioClient with credentials";
            }

            try
            {

                var att = (from r in message.dpm_attachment where r.FileName == "FAX.PDF" select r).FirstOrDefault();

                if(att == null)
                {
                    PDF.PDFLetterGenerator generator = new PDF.PDFLetterGenerator();
                    generator.Title = message.Subject;
                    generator.Text = message.Message;
                    byte[] doc = generator.GeneratePdfLetter();
                    att = new dpm_attachment
                    {
                        Content = doc,
                        FileName = "FAX.PDF",
                        MimeType = "application/pdf",
                        MessageID = message.ID
                    };

                    Model.DpMessengerEntities db = new DpMessengerEntities();
                    db.dpm_attachment.Add(att);
                    db.SaveChanges();
                    
                }


                if (att != null)
                {
                    string faxurl = Configuration.TwilioSettings.FaxDeliveryUrl.Replace("{ID}", message.ID);

                    var fax = FaxResource.Create(
                        from: Configuration.TwilioSettings.FaxSenderNumber,
                        to: message.ToPhoneNumber,
                        mediaUrl: new Uri(faxurl)
                    );
                }
                
                Success = true;
            }
            catch
            {
                Success = false;
                ErrorMessage = "Impossible to send SMS with TwilioClient";
            }


        }
    }
}
