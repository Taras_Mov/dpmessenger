﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPMessenger.Configuration;
using DPMessenger.Model;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace DPMessenger.SendingProtocols
{
    public class SMSSender : IProtocolSender
    {
        public string Protocol
        {
            get
            {
                return MessageProtocol.SMS.ToString();
            }
        }


        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public double SendingDuration { get; set; }
        public string Server { get; set; }

        public Configuration.Configuration Configuration { get; set; }
        public void Send(dpm_message message)
        {
            Server = Configuration.TwilioSettings.AccountSid + "@twilio.com";

            

            try
            {

                TwilioClient.Init(Configuration.TwilioSettings.AccountSid, Configuration.TwilioSettings.AuthToken);
            }
            catch
            {
                Success = false;
                ErrorMessage = "Impossible to init TwilioClient with credentials";
            }

            try
            {
                string text = message.Message;

                if(message.dpm_attachment.Count > 0 
                    && Configuration.MobileAttachments != null
                    && !String.IsNullOrWhiteSpace(Configuration.MobileAttachments.AttachmentsUrl))
                {
                    text += "\n\n \uD83D\uDCCE " + Configuration.MobileAttachments.AttachmentsUrl.Replace("{ID}", message.ID);
                }

                if (!String.IsNullOrEmpty(message.PossibleAnswers))
                {
                    var answers = Answers.Answer.Deserialize(message.PossibleAnswers);

                    var spRepliesMenu = Configuration.GetSpeachValue("RepliesMessage", message.Language);
                    if (spRepliesMenu != null)
                    {
                        text += "\n\n " + spRepliesMenu.Text;

                    }

                    foreach (var item in answers)
                    {
                        string label = item.GetLabel(message.Language);
                        text += "\n" + item.Number + " : " + label;
                    }

                    int delay = 15;
                    if (Configuration.InstantReplySettings != null && Configuration.InstantReplySettings.TextMessageDelayInMinutes > 0) delay = Configuration.InstantReplySettings.TextMessageDelayInMinutes;
                    DateTime before = DateTime.Now.Add(TimeSpan.FromMinutes(delay));
                    string spReplyBeforeText = "Reply before $DATE$.";
                    var spReplyBefore = Configuration.GetSpeachValue("ReplyBefore", message.Language);
                    if (spReplyBefore != null) spReplyBeforeText = spReplyBefore.Text;
                    spReplyBeforeText = spReplyBeforeText.Replace("$DATE$", before.ToString("HH:mm"));
                    text += "\n\n " + spReplyBeforeText;

                    CloseOtherOpenedSmsFor(message.ToPhoneNumber, message.ID);
                    
                }

                var sms = MessageResource.Create(
                    body: text,
                    from: new Twilio.Types.PhoneNumber(Configuration.TwilioSettings.SMSSenderNumber),
                    to: new Twilio.Types.PhoneNumber(message.ToPhoneNumber),
                    statusCallback: new Uri(Configuration.TwilioSettings.SMSStatusCallBackUrl.Replace("{ID}", message.ID))
                );

                Success = true;
            }
            catch
            {
                Success = false;
                ErrorMessage = "Impossible to send SMS with TwilioClient";
            }


        }

        private void CloseOtherOpenedSmsFor(string toPhoneNumber, string beforeMessageId)
        {
            Model.DpMessengerEntities db = new DpMessengerEntities();
            var openedMessagesToClose = (from r in db.dpm_message
                                         where r.ID != beforeMessageId
                                         && r.Protocol == "SMS"
                                         && r.ToPhoneNumber == toPhoneNumber
                                         && !String.IsNullOrEmpty(r.PossibleAnswers)
                                         && (r.AnswerClosed == null || r.AnswerClosed == 0)
                                         && r.AnswerDate == null
                                         select r).ToList();
            foreach (var m in openedMessagesToClose)
            {
                m.AnswerClosed = 1;
                m.dpm_log.Add(new dpm_log
                {
                    Date = DateTime.Now,
                    IsError = false,
                    Message = "Answers are close because a new SMS was sent to the same number " + toPhoneNumber + " - message: " + beforeMessageId
                });
            }
            db.SaveChanges();
        }
    }
}
