﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPMessenger.Configuration;
using DPMessenger.Model;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Fax.V1;
using Twilio.Types;

namespace DPMessenger.SendingProtocols
{
    public class CallSender : IProtocolSender
    {
        public string Protocol
        {
            get
            {
                return MessageProtocol.CALL.ToString();
            }
        }


        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public double SendingDuration { get; set; }
        public string Server { get; set; }

        public Configuration.Configuration Configuration { get; set; }
        public void Send(dpm_message message)
        {
            Server = Configuration.TwilioSettings.AccountSid + "@twilio.com";



            try
            {

                TwilioClient.Init(Configuration.TwilioSettings.AccountSid, Configuration.TwilioSettings.AuthToken);
            }
            catch
            {
                Success = false;
                ErrorMessage = "Impossible to init TwilioClient with credentials";
            }

            try
            {

                string callurl = Configuration.TwilioSettings.VoiceDeliveryUrl.Replace("{ID}", message.ID);

                var call = CallResource.Create(
                    from: new PhoneNumber(Configuration.TwilioSettings.VoiceSenderNumber),
                    to: new PhoneNumber(message.ToPhoneNumber),
                    url: new Uri(callurl)
                );

                Success = true;
            }
            catch
            {
                Success = false;
                ErrorMessage = "Impossible to send SMS with TwilioClient";
            }


        }
    }
}
