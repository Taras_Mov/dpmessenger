﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPMessenger.Configuration;
using DPMessenger.Model;
using System.Net.Http;
using System.Net;
using System.IO;
using Google.Apis.Auth.OAuth2;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace DPMessenger.SendingProtocols
{
    public class MobileAppFirebaseSender : IProtocolSender
    {
        public string Protocol
        {
            get
            {
                return MessageProtocol.MOBILEAPPNOTIF.ToString();
            }
        }


        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public double SendingDuration { get; set; }
        public string Server { get; set; }

        public Configuration.Configuration Configuration { get; set; }


        
        private async Task<string> GetAccessTokenAsync(string applicationName)
        {
            string json = DPMessenger.Configuration.Configuration.GetFirebaseAppToken(applicationName);
            var tmp = GoogleCredential.FromJson(json);
            tmp = tmp.CreateScoped("https://www.googleapis.com/auth/firebase.messaging");
            return await tmp.UnderlyingCredential.GetAccessTokenForRequestAsync().ConfigureAwait(false);
        }

        public string GetAccessToken(string applicationName)
        {
            return GetAccessTokenAsync(applicationName).Result;
        }


        public async Task<HttpResponseMessage> SendAync(HttpClient client, HttpRequestMessage msg)
        {
            var result = await client.SendAsync(msg).ConfigureAwait(false);
            return result;
        }

        public HttpResponseMessage Send(HttpClient client, HttpRequestMessage msg)
        {
            return SendAync(client, msg).Result;
        }

        public void Send(dpm_message message)
        {
 

            
            HttpClient client = new HttpClient();




            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            using (JsonWriter writer = new JsonTextWriter(sw))
            {


                writer.WriteStartObject();
                writer.WritePropertyName("test");
                writer.WriteValue("lapin");

                foreach (var item in message.dpm_attachment)
                {
                    try
                    {
                        if (item.MimeType == "text/plain")
                        {
                            if (item.FileName.ToLower().EndsWith(".txt"))
                                item.FileName = item.FileName.Substring(0, item.FileName.Length - 4);

                            string value = System.Text.Encoding.Default.GetString(item.Content);
                            writer.WritePropertyName(item.FileName);
                            writer.WriteValue(value);
                        }
                    }
                    catch { }
                }

                writer.WriteEndObject();
            }
            string strData = sb.ToString();




            var content = @"{
                ""message"":{
                    ""token"" : """ + message.ToMobileAppToken + @""",
                    ""notification"" : {
                        ""body"" : """ + message.Message + @""",
                        ""title"" : """ + message.Subject + @"""
                    },
                 ""data"" : " + strData + "}}";

            string url = DPMessenger.Configuration.Configuration.GetFirebaseAppUrl(message.Application);
            Server = url;

            var msg = new HttpRequestMessage()
            {
                Version = HttpVersion.Version11,
                Method = HttpMethod.Post,
                RequestUri = new Uri(url),
                Content = new StringContent(content, Encoding.UTF8, "application/json")
            };


            // a gerer en configuration
            string accessToken = GetAccessToken(message.Application);
            message.ToMobileAppToken = DPMessenger.Configuration.Configuration.GetFirebaseUserToken(message.Application, message.ToApplicationUser);
            
            // -->


            msg.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var resp = Send(client, msg);

            if(resp.IsSuccessStatusCode)
            {
                Success = true;
            }
            else
            {
                Success = false;
                ErrorMessage = resp.ToString();
            }
            
        }
    }
}
