﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using DPMessenger.Model;

namespace DPMessenger.SendingProtocols
{
    public class MailSender : IProtocolSender
    {
        public string Protocol
        {
            get
            {
                return MessageProtocol.EMAIL.ToString();
            }
        }

        #region Multiple SMTP servers management
        private static int _currentSmtp = 0;
 

        private SmtpServerAndConfig GetSmtp()
        {
            if (_currentSmtp >= Configuration.SmtpConfiguration.Servers.Count)
            {
                _currentSmtp = 0;
            }
            int i = _currentSmtp;
            if (Configuration.SmtpConfiguration.AutoBalance)
            {
                Random rdm = new Random();
                i = rdm.Next(Configuration.SmtpConfiguration.Servers.Count);
            }
            var conf =  Configuration.SmtpConfiguration.Servers[i];

            SmtpClient smtp = new SmtpClient(conf.SmtpHost, conf.SmtpPort);
            if (!String.IsNullOrEmpty(conf.SmtpLogin) || !String.IsNullOrEmpty(conf.SmtpPassword))
                smtp.Credentials = new System.Net.NetworkCredential(conf.SmtpLogin, conf.SmtpPassword);
            smtp.EnableSsl = conf.SmtpEnableSSL;
            smtp.Timeout = Configuration.SmtpConfiguration.TimeoutSeconds * 1000;


            SmtpServerAndConfig ret = new SmtpServerAndConfig
            {
                Smtp = smtp,
                Config = conf
            };


           

            return ret;
        }

        private void ChangeSmtp()
        {

            if (_currentSmtp < Configuration.SmtpConfiguration.Servers.Count)
            {
                _currentSmtp++;
            }
            else
            {
                _currentSmtp = 0;
            }
            
        }
        #endregion

        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public double SendingDuration { get; set; }
        public string Server { get; set; }

        public Configuration.Configuration Configuration { get; set; }
        public void Send(dpm_message message)
        {
            var smtp = GetSmtp();
            Server = smtp.Smtp.Host;


            string from = message.FromEmail;
            if (!String.IsNullOrWhiteSpace(smtp.Config.TrustedSender)) from = smtp.Config.TrustedSender;

            List<System.IO.MemoryStream> garbage = new List<System.IO.MemoryStream>();

            MailMessage mail = null;
            try
            {
                mail = new MailMessage();
                mail.From = new MailAddress(from,message.FromName);

                Tools.MailAddressesParser mailadresses = new Tools.MailAddressesParser();

                mailadresses.Parse(message.FromEmail);

                // from and reply to
                if (String.IsNullOrWhiteSpace(message.FromName))
                {
                    mail.From = new MailAddress(from);
                    var to = new MailAddress(message.FromEmail, message.FromName);
                    mail.ReplyToList.Add(to);
                }
                else
                {
                    mail.From = new MailAddress(from, message.FromName);
                    var to = new MailAddress(message.FromEmail, message.FromName);
                    mail.ReplyToList.Add(to);
                }
                // -->

                // to
                mailadresses.Parse(message.ToEmail);
                foreach (var recipient in mailadresses.Addresses)
                {
                    mail.To.Add(new MailAddress(recipient));
                }
                // -->


                // cc
                mailadresses.Parse(message.CcEmail);
                foreach (var recipient in mailadresses.Addresses)
                {
                    mail.CC.Add(new MailAddress(recipient));
                }
                // -->

                mail.Subject = message.Subject;
                mail.IsBodyHtml = true;
                mail.Body = "<div>"+ message.Message +"</div>";

                string answerhtml = "";
                if (!String.IsNullOrEmpty(message.PossibleAnswers))
                {
                    var answers = Answers.Answer.Deserialize(message.PossibleAnswers);

                    answerhtml += "<table style=\"width:100%; border:1px solid #333333; border-collapse:collapse; font-family:Arial, Verdana, Calibri;\" width=\"100%\" border=1>";


                    string spRepliesMenuText = "Answer by clicking on the right answer";
                    var spRepliesMenu = Configuration.GetSpeachValue("RepliesClick", message.Language);
                    if (spRepliesMenu != null)
                    {
                        spRepliesMenuText = spRepliesMenu.Text;
                    }
                    spRepliesMenuText = System.Web.HttpUtility.HtmlEncode(spRepliesMenuText);
                    answerhtml += "<tr>";
                    answerhtml += "<td style =\"background:#eeeeee; padding:5px; color:#333333; font-size:12px;\">"+ spRepliesMenuText +"</td>";
                    answerhtml += "</tr>";
                    

                    foreach (var item in answers)
                    {
                        string label = item.GetLabel(message.Language);
                        string text = item.Value;
                        if (!String.IsNullOrWhiteSpace(label)) text = label;

                        string htmlLabel = System.Web.HttpUtility.HtmlEncode(item.Number + " - " + text);

                        string link = Configuration.EmailAcknowledgement.ReplyByClickUrl;
                        link = link.Replace("{ID}", message.ID);
                        link = link.Replace("{VALUE}", System.Web.HttpUtility.UrlEncode(item.Value));

                        answerhtml += "<tr>";
                        answerhtml += "	<td><a href=\""+link+"\" style=\"background:#dedede; padding:5px; display:inline-block; border-radius:3px; margin:5px;\">"+ htmlLabel + "</a></td>";
                        answerhtml += "</tr>";
                    }

                    int delay = 15;
                    if (Configuration.InstantReplySettings != null && Configuration.InstantReplySettings.TextMessageDelayInMinutes > 0) delay = Configuration.InstantReplySettings.TextMessageDelayInMinutes;
                    DateTime before = DateTime.Now.Add(TimeSpan.FromMinutes(delay));
                    string spReplyBeforeText = "Reply before $DATE$.";
                    var spReplyBefore = Configuration.GetSpeachValue("ReplyBefore", message.Language);
                    if (spReplyBefore != null) spReplyBeforeText = spReplyBefore.Text;
                    spReplyBeforeText = spReplyBeforeText.Replace("$DATE$", before.ToString("HH:mm"));
                    spReplyBeforeText = System.Web.HttpUtility.HtmlEncode(spReplyBeforeText);

                    answerhtml += "<tr>";
                    answerhtml += "		<td style=\"background:#eeeeee; color:#666666; font-size:12px; text-align:right;\">"+ spReplyBeforeText +"</td>";
                    answerhtml += "</tr>";


                    answerhtml += "</table>";


                }


                // auto add transparent image to know if email was read
                if (Configuration.EmailAcknowledgement != null && !String.IsNullOrWhiteSpace(Configuration.EmailAcknowledgement.ImageTrackerUrl))
                {
                    if (mail.Body == null) mail.Body = "";
                    if (mail.Body.Contains("</body>"))
                    {
                        string url = Configuration.EmailAcknowledgement.ImageTrackerUrl;
                        url = url.Replace("{ID}", message.ID);
                        mail.Body = mail.Body.Replace("</body>", "<div><img src=\"" + url + "\" width=25 height=25 /></div></body>");
                    }
                    else
                    {
                        string url = Configuration.EmailAcknowledgement.ImageTrackerUrl;
                        url = url.Replace("{ID}", message.ID);
                        mail.Body = mail.Body + "<div><img src=\"" + url + "\" width=25 height=25 /></div>";
                    }
                }
                // -->

                if (!String.IsNullOrWhiteSpace(answerhtml))
                {
                    if (mail.Body == null) mail.Body = "";
                    if (mail.Body.Contains("</body>"))
                    {
                        mail.Body = mail.Body.Replace("</body>", answerhtml + "</body>");
                    }
                    else
                    {
                        mail.Body = mail.Body + answerhtml;
                    }
                }
                


                mail.Body = mail.Body.Replace("$unsubscribeUrl$", Configuration.EmailAcknowledgement.UnsubscribeUrl + "?q=" + message.ID);

                if (message.dpm_attachment != null && message.dpm_attachment.Count > 0)
                {
                    foreach (var attachment in message.dpm_attachment.ToList())
                    {
                        var ms = new System.IO.MemoryStream(attachment.Content);
                        garbage.Add(ms);

                        var att = new Attachment(ms, attachment.FileName, attachment.MimeType);
                        mail.Attachments.Add(att);
                    }
                }
            }
            catch (Exception ex)
            {
                Success = false;
                ErrorMessage = "Invalid MailMessage attributes: " + ex.Message;
                return;
            }


            try
            {
                DateTime start = DateTime.Now;
                Console.WriteLine("Mail From Address: " + mail.From.Address);
                Console.WriteLine("Mail From Name: " + mail.From.DisplayName);
                if (mail.ReplyToList.FirstOrDefault() != null)
                {
                    Console.WriteLine("Mail ReplyTo Address: " + mail.ReplyToList.FirstOrDefault().Address);
                    Console.WriteLine("Mail ReplyTo Name: " + mail.ReplyToList.FirstOrDefault().DisplayName);
                }
                smtp.Smtp.Send(mail);
                SendingDuration = Math.Round(DateTime.Now.Subtract(start).TotalSeconds, 2);
                Success = true;

            }
            catch (Exception ex)
            {
                Success = false;
                ErrorMessage = "Impossible to send email with server "+Server+" : " + ex.Message;
            }


            foreach (var item in garbage)
            {
                item.Dispose();
            }
            garbage.Clear();

        }
    }

    class SmtpServerAndConfig
    {
        public SmtpClient Smtp { get; set; }
        public Configuration.SmtpServer Config { get; set; }
    }
}
