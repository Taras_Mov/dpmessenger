﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger
{
    public class DbLogger
    {
        Model.DpMessengerEntities db;
        public DbLogger(Model.DpMessengerEntities db)
        {
            this.db = db;
        }

        public void Log(string messageId, bool success, string message)
        {
            Model.dpm_log log = new Model.dpm_log();
            log.Date = DateTime.UtcNow;
            log.MessageID = messageId;
            log.IsError = !success;
            log.Message = message;
            db.dpm_log.Add(log);
        }
    }
}
