﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DPMessenger.Webservice
{
    public class WhatsappStatusHttpHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("");

            string smsSid = context.Request.Form["SmsSid"];
            string messageStatus = context.Request.Form["MessageStatus"];
            string id = context.Request.Params["q"];

            Model.DpMessengerEntities db = new Model.DpMessengerEntities();

            if (context.Request.Form["EventType"] != null && context.Request.Form["EventType"].ToUpper() == "READ")
            {
                messageStatus = "read";
                var msg = (from r in db.dpm_message where r.ID == id select r).FirstOrDefault();
                if(msg != null && msg.SendDate != null)
                {
                    msg.AckDate = DateTime.Now;
                    db.SaveChanges();
                }
            }

            

            
            DbLogger logger = new DbLogger(db);
            logger.Log(id, true, "SMS status changed to '" + messageStatus + "' - smsSid is '" + smsSid + "'");

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}