﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace DPMessenger.Webservice
{
    public class AttachmentsHttpHandler : IHttpHandler
    {

        private string GetUserIP(HttpContext context)
        {
            string ipList = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public void ProcessRequest(HttpContext context)
        {
            string ip = GetUserIP(context);
            Security.IPLock ipl = new Security.IPLock(); 

            if(ipl.IsIpLocked(ip))
            {
                WriteHeader(context);
                context.Response.Write("<p style=\"text-align:center\"><span class=\"icon-alert\"></span></p>");
                context.Response.Write("<p style=\"text-align:center\">IP locked</p>");
                context.Response.Write("<p style=\"text-align:center\">IP vérouillée</p>");
                context.Response.Write("<p style=\"text-align:center\">IP gesperrt</p>");
                WriteFooter(context);
                return;
            }


            string message = null;

            if (context.Request.QueryString["q"] != null)
            {
                message = context.Request.QueryString["q"];
                AcknowledgementService service = new AcknowledgementService();
                service.MarkAsRead(context.Request.QueryString["q"]);

            }

            if (context.Request.QueryString["id"] == null)
            {
                WriteHeader(context);

                context.Response.ContentType = "text/html";

                var config = Configuration.Configuration.Read();
                DateTime date = new DateTime(2000, 1, 1);
                if (config != null && config.MobileAttachments != null && config.MobileAttachments.AttachmentsAvailableHours > 0)
                {
                    date = DateTime.Now.Subtract(TimeSpan.FromHours(config.MobileAttachments.AttachmentsAvailableHours));
                }


                Model.DpMessengerEntities db = new Model.DpMessengerEntities();

                var msg = (from r in db.dpm_message
                           where r.ID == message
                           && r.SendDate > date
                           select r.Message).FirstOrDefault();

                if (msg != null)
                {
                    var list = (from r in db.dpm_attachment
                                where r.MessageID == message
                                select new
                                {
                                    id = r.ID,
                                    name = r.FileName
                                }).ToList();







                    context.Response.Write("<blockquote>" + System.Web.HttpUtility.HtmlEncode(msg) + "</blockquote>");
                    context.Response.Write("\r\n");



                    context.Response.Write("<p>");
                    foreach (var item in list)
                    {

                        context.Response.Write("<a class=\"button primary bordered doc\" href=\"?q=" + message + "&id=" + item.id + "\">");
                        context.Response.Write("<span class=\"icon-link\"></span>");
                        context.Response.Write(item.name);
                        context.Response.Write("</a>");



                    }
                    context.Response.Write("</p>");
                }
                else
                {
                    context.Response.Write("<p style=\"text-align:center\"><span class=\"icon-alert\"></span></p>");
                    context.Response.Write("<p style=\"text-align:center\">No more available</p>");
                    context.Response.Write("<p style=\"text-align:center\">Plus disponible</p>");
                    context.Response.Write("<p style=\"text-align:center\">nicht mehr verfügbar</p>");

                    ipl.RegisterWrongAccess(ip);
                }
                WriteFooter(context);

            }
            else
            {
                long id = -1;
                long.TryParse(context.Request.QueryString["id"].ToString(), out id);

                Model.DpMessengerEntities db = new Model.DpMessengerEntities();
                var att = (from r in db.dpm_attachment where r.MessageID == message && r.ID == id select r).FirstOrDefault();

                if(att != null)
                {
                    context.Response.ContentType = att.MimeType;
                    context.Response.OutputStream.Write(att.Content, 0, att.Content.Length);
                }
            }
        }

        private static void WriteFooter(HttpContext context)
        {
            context.Response.Write("</body>");
        }

        private static void WriteHeader(HttpContext context)
        {
            context.Response.Write("<head>");
            context.Response.Write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" />");
            context.Response.Write("<style>");
            context.Response.Write(Properties.Resources.minicss);
            context.Response.Write("</style>");
            context.Response.Write("</head>");
            context.Response.Write("\r\n");
            context.Response.Write("<body>");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
