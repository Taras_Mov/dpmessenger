﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DPMessenger.Webservice
{
    public class WhatsappReceiverHttpHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var config = DPMessenger.Configuration.Configuration.Read();

            if (context.Request.Params["Body"] != null && context.Request.Params["From"] != null)
            {
                string message = context.Request.Params["Body"];
                string fromphone = context.Request.Params["From"];
                string tophone = "";
                if(context.Request.Params["To"] != null) tophone = context.Request.Params["To"];

                fromphone = fromphone.Replace("whatsapp:", "");
                tophone = tophone.Replace("whatsapp:", "");

                int delay = 15;
                if (config.InstantReplySettings != null && config.InstantReplySettings.TextMessageDelayInMinutes > 0) delay = config.InstantReplySettings.TextMessageDelayInMinutes;
                DateTime after = DateTime.Now.Subtract(TimeSpan.FromMinutes(delay));

                Model.DpMessengerEntities db = new Model.DpMessengerEntities();
                var originalmsg = (from r in db.dpm_message
                                   where r.Protocol == "WHATSAPP" && r.ToPhoneNumber == fromphone
                                   && r.SendDate != null
                                   && r.SendDate > after
                                   && !String.IsNullOrEmpty(r.PossibleAnswers)
                                   && (r.AnswerClosed == 0 ||r.AnswerClosed == null)
                                   orderby r.SendDate descending
                                   select r).FirstOrDefault();

                if (originalmsg != null)
                {
                    var answers = Answers.Answer.Deserialize(originalmsg.PossibleAnswers);

                    bool notvalidReply = true;

                    string reply = message.Trim();
                    int numReply = -1;
                    if (Int32.TryParse(reply, out numReply))
                    {

                        var choicedAnswer = (from r in answers where r.Number == numReply select r).FirstOrDefault();
                        notvalidReply = choicedAnswer == null;

                        if (!notvalidReply)
                        {
                            DateTime dtn = DateTime.Now;
                            originalmsg.AnswerDate = dtn;
                            originalmsg.AnswerValue = choicedAnswer.Value;
                            originalmsg.dpm_log.Add(new Model.dpm_log
                            {
                                Date = dtn,
                                IsError = false,
                                Message = "Answered by WHATSAPP from  " + fromphone + " - content \"" + message + "\" - matched to value: " + choicedAnswer.Value
                            });
                            db.SaveChanges();

                            

                            string response = "";

                            var spSentence = config.GetSpeachValue("ReplyConfirmed", originalmsg.Language);
                            if (spSentence != null)
                            {
                                response = spSentence.Text;
                            }

                            string choicedAnswerToSay = choicedAnswer.GetLabel(originalmsg.Language);
                            response += "\n" + choicedAnswerToSay;

                            spSentence = config.GetSpeachValue("Thanks", originalmsg.Language);
                            if (spSentence != null)
                            {
                                response += "\n\n" + spSentence.Text;
                            }

                            DpPostMan sender = new DpPostMan();
                            NewMessage msgreply = new NewMessage();
                            msgreply.Init(originalmsg.Application, originalmsg.ToApplicationOrganization, originalmsg.ToApplicationUser, originalmsg.MessageType + "_Reply");
                            msgreply.SetWhatsapp(fromphone, response);
                            sender.SendNow(msgreply);

                            // send response directly

                        }

                        




                    }

                    if (notvalidReply)
                    {
                        string response = "Not valid reponse";
                        var spSentence = config.GetSpeachValue("ReplyNotFound", originalmsg.Language);
                        if (spSentence != null)
                        {
                            response = spSentence.Text;
                        }
                        DpPostMan sender = new DpPostMan();
                        NewMessage msgreply = new NewMessage();
                        msgreply.Init(originalmsg.Application, originalmsg.ToApplicationOrganization, originalmsg.ToApplicationUser, originalmsg.MessageType + "_Reply");
                        msgreply.SetWhatsapp(fromphone, response);
                        sender.SendNow(msgreply);

                    }

                    
                    db.SaveChanges();
                    OnMessage(originalmsg, fromphone, message);
                }
            }
        }

        public virtual void OnMessage(Model.dpm_message message, string fromPhoneNumber, string text)
        {

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    
    }
}
