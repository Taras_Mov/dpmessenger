﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using Twilio.TwiML;
using Twilio.TwiML.Voice;

namespace DPMessenger.Webservice
{
    public class CallHttpHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Params["q"] != null)
            {
                string messageId = context.Request.Params["q"];


                if (context.Request.UserAgent.StartsWith("TwilioProxy/"))
                {
                    Model.DpMessengerEntities db = new Model.DpMessengerEntities();
                    Model.dpm_message message = (from r in db.dpm_message where r.ID == messageId select r).FirstOrDefault();
                    if (String.IsNullOrEmpty(message.Language)) message.Language = "fr-FR";

                    if (context.Request.Params["gather"] != null && context.Request.Params["gather"] == "1")
                    {
                        OnResponse(db, message, context);
                    }
                    else
                    {

                        CallStart(db, message, context);
                    }

                }
            }
        }


       

        private void CallStart(Model.DpMessengerEntities db, Model.dpm_message message, HttpContext context)
        {
            var response = new VoiceResponse();

            var config = DPMessenger.Configuration.Configuration.Read();

            if (!String.IsNullOrWhiteSpace(config.SpeachSettings.SoundOpenerMp3Url)) response.Play(new Uri(config.SpeachSettings.SoundOpenerMp3Url));
            response.Say(message.Message, voice: "alice", language: message.Language);
            

            ImplementResponseMenu(response, message);


            string xml = response.ToString();
            context.Response.ContentType = "text/xml";
            context.Response.Write(xml);
        }

        private void ImplementResponseMenu(VoiceResponse response, Model.dpm_message message)
        {


            if(!String.IsNullOrWhiteSpace(message.PossibleAnswers))
            {
                var answers = Answers.Answer.Deserialize(message.PossibleAnswers);

                var config = DPMessenger.Configuration.Configuration.Read();
                string gatherUrl = config.TwilioSettings.VoiceDeliveryUrl;
                gatherUrl = gatherUrl.Replace("{ID}", message.ID);
                gatherUrl += "&gather=1";

                var gather = new Gather(action: new Uri(gatherUrl), method: Twilio.Http.HttpMethod.Get);

                var spRepliesMenu = config.GetSpeachValue("RepliesMenu", message.Language);
                if (spRepliesMenu != null)
                {
                    gather.Say(spRepliesMenu.Text, voice: "alice", language: spRepliesMenu.Language);
                }

                foreach (var item in answers)
                {
                    string label = item.GetLabel(message.Language);
                    gather.Say(item.Number + ": "+label+"!", voice: "alice", language: message.Language);
                }
                
            

                response.Append(gather);

                

            }




           
        }


        private void OnResponse(Model.DpMessengerEntities db, Model.dpm_message message, HttpContext context)
        {
            var response = new VoiceResponse();
            var config = DPMessenger.Configuration.Configuration.Read();

            bool noreply = false;

            if (!String.IsNullOrWhiteSpace(message.PossibleAnswers) && context.Request.Params["Digits"] != null)
            {
                var answers = Answers.Answer.Deserialize(message.PossibleAnswers);
                string reply = context.Request.Params["Digits"];
                int numReply = -1;
                if(Int32.TryParse(reply, out numReply))
                {

                    var choicedAnswer = (from r in answers where r.Number == numReply select r).FirstOrDefault();
                    noreply = choicedAnswer == null;

                    if(!noreply)
                    {
                        DateTime dt = DateTime.Now;
                        message.AnswerDate = dt;
                        message.AnswerValue = choicedAnswer.Value;
                        message.dpm_log.Add(new Model.dpm_log
                        {
                            Date = dt,
                            IsError = false,
                            Message = "Answered by phone " + reply + "# - matched to value: " + choicedAnswer.Value
                        });
                        db.SaveChanges();

                        var spSentence = config.GetSpeachValue("ReplyConfirmed", message.Language);
                        if (spSentence != null)
                        {
                            response.Say(spSentence.Text, voice: "alice", language: spSentence.Language);
                        }

                        string choicedAnswerToSay = choicedAnswer.GetLabel(message.Language);
                        response.Say(choicedAnswerToSay + "!", voice: "alice", language: spSentence.Language);

                        spSentence = config.GetSpeachValue("Thanks", message.Language);
                        if (spSentence != null)
                        {
                            response.Say(spSentence.Text, voice: "alice", language: spSentence.Language);
                        }


                    }
                   
                }

                

            }


            if (noreply)
            {
                var spSentence = config.GetSpeachValue("ReplyNotFound", message.Language);
                if (spSentence != null)
                {
                    response.Say(spSentence.Text, voice: "alice", language: spSentence.Language);
                }

                ImplementResponseMenu(response, message);
            }

            

            string xml = response.ToString();
            context.Response.ContentType = "text/xml";
            context.Response.Write(xml);
        }




        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
