﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DPMessenger.Webservice
{
    public class ImageTrackerHttpHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "image/png";

            if(context.Request.QueryString["q"] != null)
            {
                AcknowledgementService service = new AcknowledgementService();
                service.MarkAsRead(context.Request.QueryString["q"]);

            }


            Bitmap bmp = new Bitmap(25, 25);
            Graphics g = Graphics.FromImage(bmp);

            g.Clear(Color.Transparent);
            g.FillRectangle(Brushes.Transparent, 0, 0, 25, 25);

            g.Flush();
            bmp.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Png);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
