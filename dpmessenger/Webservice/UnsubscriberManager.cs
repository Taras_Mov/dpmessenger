﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.Webservice
{
    public class UnsubscriberManager
    {
        public UnsubscriptionInfo GetInfo(string token)
        {
            UnsubscriptionInfo ret = new UnsubscriptionInfo();
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var m = (from r in db.dpm_message where r.ID == token select r).FirstOrDefault();
            if(m!=null && !String.IsNullOrWhiteSpace(m.ToPhoneNumber))
            {
                ret.CanUnsubscribe = true;
                ret.Recipient = m.ToPhoneNumber;
            }
            if (m != null && !String.IsNullOrWhiteSpace(m.ToEmail))
            {
                ret.CanUnsubscribe = true;
                ret.Recipient = m.ToEmail;
            }
            return ret;
        }

        public bool RegisterForbiddenRecipient(string token, string messagetype)
        {
            bool ret = false;
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();

            if(String.IsNullOrEmpty(messagetype))
            {
                messagetype = (from r in db.dpm_message where r.ID == token select r.MessageType).FirstOrDefault();
            }

            var m = (from r in db.dpm_message where r.ID == token select r).FirstOrDefault();
            if(m != null)
            {
                var r = GetInfo(token);

                db.dmp_blacklist.Add(new Model.dmp_blacklist
                {
                    Application = m.Application,
                    Forbidden = r.Recipient,
                    MessageType = messagetype,
                    Protocol = m.Protocol
                });

                db.SaveChanges();

                ret = true;
            }
            return ret;
        }
    }

    public class UnsubscriptionInfo
    {
        public bool CanUnsubscribe { get; set; }
        public string Recipient { get; set; }

        public string AnonymousRecipient
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(Recipient))
                {
                    string ano = "";
                    string[] parts = Recipient.Split('@');

                    string left = "";
                    string right = "";
                    string separator = "";

                    if(parts.Length == 2)
                    {
                        left = parts[0];
                        right = parts[1];
                        separator = "@";
                    }
                    else
                    {
                        left = "";
                        right = Recipient;
                        separator = "";
                    }

                    left = left.Replace(" ", "").Trim();
                    right = right.Replace(" ", "").Trim();

                    if (!String.IsNullOrEmpty(left) && left.Length>3)
                    {
                        string anoleft = left.Substring(0, 4);
                        while (anoleft.Length < left.Length) anoleft += "*";
                        left = anoleft;
                    }
                    if (!String.IsNullOrEmpty(right) && right.Length > 3)
                    {
                        string anoright = right.Substring(right.Length - 4);
                        while (anoright.Length < right.Length) anoright = "*" + anoright;
                        right = anoright;
                    }


                    ano = left + separator + right;

                    return ano;
                }
                else
                {
                    return null;
                }
                
            }
        }



    }
}
