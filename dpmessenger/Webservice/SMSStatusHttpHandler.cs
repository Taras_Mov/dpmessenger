﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DPMessenger.Webservice
{
    public class SMSStatusHttpHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("");

            string smsSid = context.Request.Form["SmsSid"];
            string messageStatus = context.Request.Form["MessageStatus"];
            string id = context.Request.Params["q"];

            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            DbLogger logger = new DbLogger(db);
            logger.Log(id, true, "SMS status changed to '" + messageStatus + "' - smsSid is '" + smsSid + "'");

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}