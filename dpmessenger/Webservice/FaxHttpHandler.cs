﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DPMessenger.Webservice
{
    public class FaxHttpHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Params["q"] != null)
            {

                string userHostAddress = context.Request.UserHostAddress;

                string messageId = context.Request.Params["q"];

                Model.DpMessengerEntities db = new Model.DpMessengerEntities();

                byte[] data = (from r in db.dpm_attachment where r.MessageID == messageId && r.FileName == "FAX.PDF" select r.Content).FirstOrDefault();
                if (data != null)
                {
                    context.Response.ContentType = "application/pdf";
                    context.Response.OutputStream.Write(data, 0, data.Length);
                }
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
