﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger
{
    public class NewMessage
    {
        public string ID { get; private set; }
        public NewMessage()
        {
            ID = Guid.NewGuid().ToString("n");
            AllowMultiple = true;
            AllowMultipleFrequency = TimeSpan.MaxValue;
        }
        public string Application { get; set; }
        public string Type { get; set; }

        public string CustomItemId { get; set; }

        public bool AllowMultiple { get; set; }
        public TimeSpan AllowMultipleFrequency { get; set; }

        public string ToApplicationOrganizationId { get; set; }
        public string ToApplicationUserId { get; set; }
        public string ToPhoneNumber { get; set; }
        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public string ToEmail { get; set; }
        public string CcEmail { get; set; }
        public string ToMobileAppToken { get; set; }

        public MessageProtocol Protocol { get; set; }

        public string Subject { get; set; }
        public string Body { get; set; }

        public string Language { get; set; }

        public List<Answers.Answer> PossibleAnswers { get; set; }


        public List<NewAttachment> Attachments { get; set; }


        public void Init(string applicationid, string organizationId, string toUserId, string messageType, string customItemId = null)
        {
            Application = applicationid;
            ToApplicationOrganizationId = organizationId;
            ToApplicationUserId = toUserId;
            Type = messageType;
            CustomItemId = customItemId;
        }

        public void SetMobileAppNotif(string subject, string body)
        {
            Protocol = MessageProtocol.MOBILEAPPNOTIF;
            Subject = subject;
            Body = body;
            
        }

        public void SetEmail(string from, string to, string cc, string subject, string body, string lang = null, List<Answers.Answer> choices = null)
        {
            Protocol = MessageProtocol.EMAIL;

            Tools.MailAddressesParser parser = new Tools.MailAddressesParser();
            parser.Parse(from);
            FromEmail = parser.AddressesAsString;
            

            parser.Parse(to);
            ToEmail = parser.AddressesAsString;

            parser.Parse(cc);
            CcEmail = parser.AddressesAsString;

            Subject = subject;
            Body = body;

            Language = lang;

            if (choices != null)
            {
                PossibleAnswers = choices;
            }
        }

        public void SetFaxFromPDF(string to, string pdfFile)
        {
            Protocol = MessageProtocol.FAX;

            to = to.Replace(" ", "").Trim();
            ToPhoneNumber = to;


            AddAttachement(pdfFile, AttachmentType.PDF, "FAX.PDF");

        }

        public void SetFaxFromText(string to, string title, string text)
        {
            Protocol = MessageProtocol.FAX;

            to = to.Replace(" ", "").Trim();
            ToPhoneNumber = to;


            Subject = title;
            Body = text;
        }


        public void SetCall(string to, string text, string lang = null, List<Answers.Answer> choices = null)
        {
            Protocol = MessageProtocol.CALL;

            to = to.Replace(" ", "").Trim();
            ToPhoneNumber = to;

            Body = text;
            Language = lang; 

            if(choices != null)
            {
                PossibleAnswers = choices;
            }
        }

        public void SetEmailFromHtml(string from, string to, string cc, string subject, string htmlString, Dictionary<string, string> variables)
        {
            if(variables != null)
            {
                foreach (var item in variables.Keys)
                {
                    htmlString = htmlString.Replace("$" + item + "$", variables[item]);
                }
            }
            SetEmail(from, to, cc, subject, htmlString);
        }
        public void SetEmailFromHtmlFile(string from, string to, string cc, string subject, string htmlFile, Dictionary<string, string> variables)
        {
            string htmlString = System.IO.File.ReadAllText(htmlFile);
            SetEmailFromHtml(from, to, cc, subject, htmlString, variables);
        }

        public void SetSMS(string to, string message, string lang = null, List<Answers.Answer> choices = null)
        {
            to = to.Replace(" ", "").Trim();
            Protocol = MessageProtocol.SMS;
            ToPhoneNumber = to;
            Body = message;

            Language = lang;

            if (choices != null)
            {
                PossibleAnswers = choices;
            }
        }

        public void SetWhatsapp(string to, string message, string lang = null, List<Answers.Answer> choices = null)
        {
            to = to.Replace(" ", "").Trim();
            Protocol = MessageProtocol.WHATSAPP;
            ToPhoneNumber = to;
            Body = message;

            Language = lang;

            if (choices != null)
            {
                PossibleAnswers = choices;
            }
        }

        public void AddAttachement(byte[] content, AttachmentType type, string name)
        {
            if (Attachments == null) Attachments = new List<NewAttachment>();
            Attachments.Add(new NewAttachment
            {
                Content = content,
                Type = type,
                Name = name
            });
        }

        public void AddTextAttachment(string key, string value)
        {
            AddAttachement(Encoding.Default.GetBytes(value), AttachmentType.TXT, key);
        }

        public void AddAttachement(string file, AttachmentType type, string name)
        {
            if (System.IO.File.Exists(file))
            {
                byte[] data = System.IO.File.ReadAllBytes(file);

                this.AddAttachement(data, type, name);

            }
            else
            {
                Log.LogEvent("PostMan", false, "File '" + file + "' was not found.");

            }
        }

        public void AddAttachement(string file, string name)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(file);
            string ext = fi.Extension.ToString().ToUpper().Replace(".", "");
            if (ext == "JPEG") ext = "JPG";

            AttachmentType att = AttachmentType.ZIP;
            try
            {
                att = (AttachmentType)Enum.Parse(typeof(AttachmentType), ext, true);
                Log.LogEvent("PostMan", true, "File extension " + ext + " was recognized as " + att.ToString() + ".");
            }
            catch
            {
                Log.LogEvent("PostMan", false, "File extension " + ext + " was not recognized as a known type.");

            }





            AddAttachement(file, att, name);

        }

        public void AddAttachement(string file)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(file);
            AddAttachement(fi.FullName, fi.Name);

        }

        
    }

    public enum MessageProtocol
    {
        EMAIL,
        SMS,
        WHATSAPP,
        CALL,
        MOBILEAPPNOTIF,
        FAX,
        DROPBOX,
        GDRIVE,
        ONEDRIVE
    }

    public enum AttachmentType
    {
        PDF,
        DOC,
        DOCX,
        XLS,
        XLSX,
        ZIP,
        TXT,
        RTF,
        WAV,
        MP3,
        JPG,
        PNG,
        GIF,
        BMP
    }

    public class NewAttachment
    {
        public byte[] Content { get; set; }
        public AttachmentType Type { get; set; }
        public string Name { get; set; }
    }
}

