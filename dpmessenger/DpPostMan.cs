﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger
{
    public class DpPostMan
    {
        Configuration.Configuration _config;
        public DpPostMan()
        {
            _config = Configuration.Configuration.Read();
        }
        public void SendNow(NewMessage m)
        {
            if(m.ToPhoneNumber != null)
            {
                m.ToPhoneNumber = m.ToPhoneNumber.Replace(" ", "").Trim();
            }

            Enqueue(m, DateTime.Now);
        }

        public void Enqueue(NewMessage m, DateTime scheduleDate)
        {
            if(m.Application == null)
            {
                Log.LogEvent("PostMan", false, "Application property is not set. You must define your application identifier.");
                throw new Exception("Application property is not set. You must define your application identifier.");
            }
            if (m.ToApplicationOrganizationId == null)
            {
                Log.LogEvent("PostMan", false, "ToApplicationOrganizationId property is not set. You must set your user organization id.");
                throw new Exception("ToApplicationOrganizationId property is not set. You must set your user organization id.");
            }
            if (m.ToApplicationUserId == null)
            {
                Log.LogEvent("PostMan", false, "ToApplicationUserId property is not set. You must set your user identifier.");
                throw new Exception("ToApplicationUserId property is not set. You must set your user identifier.");
            }

            Model.DpMessengerEntities db = new Model.DpMessengerEntities();

            if(m.AllowMultiple == false || m.AllowMultipleFrequency < TimeSpan.MaxValue)
            {
                Log.LogEvent("PostMan", true, "This message can not allow multiple sent. Check if already sent.");
                var q = (from r in db.dpm_message
                         where r.Application == m.Application
                         && r.ToApplicationOrganization == m.ToApplicationOrganizationId
                         && r.ToApplicationUser == m.ToApplicationUserId
                         && r.MessageType == m.Type
                         && (r.Canceled == null || r.Canceled == 0)
                         select r);
                if(m.AllowMultipleFrequency < TimeSpan.MaxValue)
                {
                    Log.LogEvent("PostMan", true, "Only check where message scheduled after " + m.AllowMultipleFrequency.ToString() + " for now");

                    DateTime dtstart = DateTime.Now.Subtract(m.AllowMultipleFrequency);
                    q = (from r in q where r.ScheduleDate > dtstart select r);
                }
                int already = q.Count();
                if (already > 0)
                {
                    Log.LogEvent("PostMan", true, "This message has already been " + already + " scheduled, so cancel.");
                    return;
                }
                else
                {
                    Log.LogEvent("PostMan", true, "This message has neved been scheduled, so continue.");
                }
            }

            var mess = new Model.dpm_message();
            mess.Application = m.Application;
            mess.Canceled = 0;
            mess.ID = m.ID;
            mess.MessageType = m.Type;
            mess.Message = m.Body;
            mess.NeedAnswer = 0;
            mess.PossibleAnswers = null;
            mess.Protocol = m.Protocol.ToString();
            mess.ScheduleDate = scheduleDate;
            mess.SendDate = null;
            mess.SendErrorRetries = 0;
            mess.Subject = m.Subject;
            mess.ToApplicationOrganization = m.ToApplicationOrganizationId;
            mess.ToApplicationUser = m.ToApplicationUserId;
            mess.FromEmail = m.FromEmail;
            mess.FromName = m.FromName;
            mess.ToEmail = m.ToEmail;
            mess.CcEmail = m.CcEmail;
            mess.ToMobileAppToken = m.ToMobileAppToken;
            mess.ToPhoneNumber = m.ToPhoneNumber;
            mess.Language = m.Language;
            if(m.PossibleAnswers != null && m.PossibleAnswers.Count > 0)
            {
                mess.PossibleAnswers = Answers.Answer.Serialize(m.PossibleAnswers);
            }


            if(m.Attachments != null && m.Attachments.Count > 0)
            {
                foreach (var att in m.Attachments)
                {
                    mess.dpm_attachment.Add(new Model.dpm_attachment
                    {
                        Content = att.Content,
                        FileName = att.Name,
                        MimeType = Tools.MimeTypeManagement.ToMime(att.Type)
                    });
                }
            }


            bool forbidden = false;
            if(!String.IsNullOrWhiteSpace(mess.ToEmail))
            {
                if((from r in db.dmp_blacklist
                 where r.Application == mess.Application
                 && mess.MessageType.StartsWith(r.MessageType)
                 && r.Forbidden == mess.ToEmail
                 select r).Count() > 0)
                {
                    forbidden = true;
                }
            }
            else if (!String.IsNullOrWhiteSpace(mess.ToPhoneNumber))
            {
                if ((from r in db.dmp_blacklist
                     where r.Application == mess.Application
                     && mess.MessageType.StartsWith(r.MessageType)
                     && r.Forbidden == mess.ToPhoneNumber
                     select r).Count() > 0)
                {
                    forbidden = true;
                }
            }

            if (!forbidden)
            {


                db.dpm_message.Add(mess);

                DbLogger logger = new DbLogger(db);
                logger.Log(mess.ID, true, "Message enqueued. Can be send from " + mess.ScheduleDate);
                Log.LogEvent("PostMan", true, "Message " + mess.ID + " enqueued. Can be send from " + mess.ScheduleDate);



                db.SaveChanges();
            }
            else
            {
                Log.LogEvent("PostMan", true, "Message " + mess.ID + " is not enqueued because recipient has unsubscribed");
            }
            
        }

        public void CancelMessages(string applicationid, string organizationId, string toUserId, string messageType)
        {
            Log.LogEvent("PostMan", true, "Canceling message of app=" + applicationid + ", org="+organizationId+ ", user=" + toUserId + ", type=" + messageType);
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var tocancel =  (from r in db.dpm_message
             where r.SendDate == null
             && (r.Canceled == 0 || r.Canceled == null)
             && r.Application == applicationid
             && r.ToApplicationOrganization == organizationId
             && r.ToApplicationUser == toUserId
             && r.MessageType == messageType
             
             select r).ToList();
            Log.LogEvent("PostMan", true, "To cancel messages found: " + tocancel.Count);
            foreach (var item in tocancel)
            {
                item.Canceled = 1;
                Log.LogEvent("PostMan", true, "Message canceled: " + item.ID);
                db.SaveChanges();
            }


        }
    }
}
