﻿using DPMessenger.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DPMessenger.Answers
{
    public class Answer
    {
        [XmlAttribute()]
        public int Number { get; set; }
        [XmlAttribute()]
        public string Value { get; set; }

        public List<VoiceItem> Label { get; set; }
        public Answer()
        {
            Label = new List<VoiceItem>(); 
        }

        public Answer(int number, string value, string language, string text)
        {
            this.Number = number;
            this.Value = value;
            this.Label = new List<VoiceItem>();
            this.Label.Add(new VoiceItem
            {
                Language = language,
                Text = text
            });
        }

        public string GetLabel(string language)
        {
            string label = (from r in this.Label where r.Language == language select r.Text).FirstOrDefault();

            if (label == null)
            {
                string language2 = language.ToLower();
                label = (from r in this.Label where r.Language.ToLower().StartsWith(language2) || language2.StartsWith(r.Language.ToLower()) select r.Text).FirstOrDefault();

            }

            if (label == null)
            {
                var def = this.Label.FirstOrDefault();
                if (def != null) label = def.Text;
            }

            if (label == null) label = this.Value;

            return label;
        }


        public static string Serialize(List<Answer> o)
        {
            string ret = null;
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializer ser = new XmlSerializer(o.GetType());
                XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                xtw.Indentation = 4;
                ser.Serialize(xtw, o);
                byte[] data = ((MemoryStream)xtw.BaseStream).ToArray();
                ret = Encoding.UTF8.GetString(data);
                ret = ret.Trim();
                int start = ret.IndexOf("<?");
                ret = ret.Substring(start);
            }
            return ret;
        }

        public static List<Answer> Deserialize(string xml)
        {

            Stream s = new MemoryStream(Encoding.UTF8.GetBytes(xml));

            Exception e = null;
            XmlSerializer ser = null;
            object o = null;
            try
            {
                ser = new XmlSerializer(typeof(List<Answer>));
                o = ser.Deserialize(s);
            }
            catch (Exception ex)
            {
                e = ex;
            }
            s.Dispose(); // end stream
            if (e != null) throw e;
            return (List<Answer>)o;
        }
    }

    
}
