﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.Query
{
    public class MessageInfo
    {
        public Model.dpm_message Message { get; set; }
        public string AnswerText { get; set; }
        public MessageInfo(string id)
        {
            Refresh(id);
        }

        public void Refresh()
        {
            Refresh(Message.ID);
        }

        public void Refresh(string id)
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            Message = (from r in db.dpm_message where r.ID == id select r).FirstOrDefault();
            if (Message != null)
            {
                if (!String.IsNullOrWhiteSpace(Message.AnswerValue) && !String.IsNullOrWhiteSpace(Message.PossibleAnswers))
                {
                    var answers = Answers.Answer.Deserialize(Message.PossibleAnswers);
                    var answer = (from r in answers where r.Value == Message.AnswerValue select r).FirstOrDefault();
                    if (Message.Language == null) Message.Language = "fr-FR";
                    AnswerText = answer.GetLabel(Message.Language);

                }
            }
        }

        public bool IsPossibleAnswer(string value)
        {
            if (!String.IsNullOrWhiteSpace(Message.PossibleAnswers))
            {
                if (Message.AnswerDate == null || DateTime.Now.Subtract(Message.AnswerDate.Value).TotalMinutes<2 )
                {

                    int delay = 15;
                    var config = Configuration.Configuration.Read();
                    if (config.InstantReplySettings != null && config.InstantReplySettings.TextMessageDelayInMinutes > 0) delay = config.InstantReplySettings.TextMessageDelayInMinutes;
                    DateTime after = DateTime.Now.Subtract(TimeSpan.FromMinutes(delay));

                    if (Message.SendDate > after)
                    {

                        var answers = Answers.Answer.Deserialize(Message.PossibleAnswers);
                        var answer = (from r in answers where r.Value == value select r).FirstOrDefault();
                        if (answer != null) return true;
                    }
                }
            }
            return false;
        }

        public void SetAnswer(string value)
        {
            DateTime dt = DateTime.Now;

            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            Message = (from r in db.dpm_message where r.ID == Message.ID select r).FirstOrDefault();
            Message.AnswerValue = value;
            Message.AnswerDate = dt;
            Message.dpm_log.Add(new Model.dpm_log
            {
                Date = dt,
                IsError = false,
                Message = "Answered set by value: " + value
            });
            db.SaveChanges();
            Refresh();
        }

        public string GetSentence(string name)
        {
            var conf = Configuration.Configuration.Read();
            var r = conf.GetSpeachValue(name, Message.Message);
            if (r != null) return r.Text;
            return name;
        }
  
    }
}
