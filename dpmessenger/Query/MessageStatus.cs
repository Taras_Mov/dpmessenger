﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.Query
{
    public class MessageStatus : MessageUserInfo
    {
        public DateTime? AckDate { get; private set; }
        public bool AnswerClosed { get; private set; }
        public DateTime? AnswerDate { get; private set; }
        public string AnswerValue { get; private set; }
        public string Application { get; private set; }
        public bool Canceled { get; private set; }
        public string CcEmail { get; private set; }
        public string CustomItemId { get; private set; }
        public string FromEmail { get; private set; }
        public string FromName { get; private set; }
        public string ID { get; private set; }
        public string Language { get; private set; }
        public string Message { get; private set; }
        public string MessageType { get; private set; }
        public bool NeedAnswer { get; private set; }
        public string Protocol { get; private set; }
        public DateTime ScheduleDate { get; private set; }
        public DateTime? SendDate { get; private set; }
        public int SendErrorRetries { get; private set; }
        public double SendingDuration { get; private set; }
        public string SendServer { get; private set; }
        public string Subject { get; private set; }
        public string ToApplicationOrganization { get; private set; }
        public string ToApplicationUser { get; private set; }
        public string ToEmail { get; private set; }
        public string ToMobileAppToken { get; private set; }
        public string ToPhoneNumber { get; private set; }


        

        public MessageStatus()
        {

        }

        public MessageStatus(Model.dpm_message m)
        {
            AckDate = m.AckDate;
            AnswerClosed = m.AnswerClosed != null && m.AnswerClosed > 0;
            AnswerDate = m.AnswerDate;
            AnswerValue = m.AnswerValue;
            Application = m.Application;
            Canceled = m.Canceled != null && m.Canceled > 0;
            CcEmail = m.CcEmail;
            CustomItemId = m.CustomItemId;
            FromEmail = m.FromEmail;
            FromName = m.FromName;
            ID = m.ID;
            Language = m.Language;
            Message = m.Message;
            MessageType = m.MessageType;
            NeedAnswer = m.NeedAnswer != null && m.NeedAnswer > 0;
            Protocol = m.Protocol;
            ScheduleDate = m.ScheduleDate;
            SendDate = m.SendDate;
            if(m.SendErrorRetries != null) SendErrorRetries = m.SendErrorRetries.Value;
            if (m.SendingDuration != null) SendingDuration = m.SendingDuration.Value;
            SendServer = m.SendServer;
            Subject = m.Subject;
            ToApplicationOrganization = m.ToApplicationOrganization;
            ToApplicationUser = m.ToApplicationUser;
            ToEmail = m.ToEmail;
            ToMobileAppToken = m.ToMobileAppToken;
            ToPhoneNumber = m.ToPhoneNumber;
            
        }

        
    }

    public class MessageUser : MessageUserInfo
    {
        public string Application { get; set; }
        public string ToApplicationOrganization { get; set; }
        public string ToApplicationUser { get; set; }

        
    }

    public class MessageUserInfo
    {
        public string CustomFirstName { get; set; }
        public string CustomLastName { get; set; }
        public string CustomNumber { get; set; }
        public string CustomRole { get; set; }
        public string CustomCompany { get; set; }
        public string CustomExtra1 { get; set; }
        public string CustomExtra2 { get; set; }
        public void AddCustomData(string customFirstName, string customLastName,
            string customNumber = null, string customRole = null,
            string customCompany = null,
            string customExtra1 = null, string customExtra2 = null)
        {
            this.CustomFirstName = customFirstName;
            this.CustomLastName = customLastName;
            this.CustomNumber = customNumber;
            this.CustomRole = customRole;
            this.CustomCompany = customCompany;
            this.CustomExtra1 = customExtra1;
            this.CustomExtra2 = customExtra2;
        }
    }
}
