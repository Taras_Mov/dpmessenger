﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.Query
{
    public class MessageStatusQuerier
    {
        public string FilterApplicationId { get; set; }
        public string FilterOrganizationId { get; set; }
        public string FilterToUserId { get; set; }
        public string FilterMessageType { get; set; }
        public string FilterCustomId { get; set; }

        public DateTime? FilterFromScheduleDate { get; set; }
        public DateTime? FilterToScheduleDate { get; set; }


        private IQueryable<Model.dpm_message> Query()
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();

            var q = (from r in db.dpm_message select r);

            if (!String.IsNullOrWhiteSpace(FilterApplicationId)) q = (from r in q where r.Application == FilterApplicationId select r);
            if (!String.IsNullOrWhiteSpace(FilterOrganizationId)) q = (from r in q where r.ToApplicationOrganization == FilterOrganizationId select r);
            if (!String.IsNullOrWhiteSpace(FilterToUserId)) q = (from r in q where r.ToApplicationUser == FilterToUserId select r);
            if (!String.IsNullOrWhiteSpace(FilterMessageType)) q = (from r in q where r.MessageType == FilterMessageType select r);
            if (!String.IsNullOrWhiteSpace(FilterCustomId)) q = (from r in q where r.CustomItemId == FilterMessageType select r);
            if (FilterFromScheduleDate != null) q = (from r in q where r.ScheduleDate >= FilterFromScheduleDate.Value select r);
            if (FilterToScheduleDate != null) q = (from r in q where r.ScheduleDate <= FilterToScheduleDate.Value select r);

            return q;
        }

        public long Count()
        {
            var q = Query();
            return q.LongCount();
        }

        public List<MessageUser> GetDistinctUsers(int skip = 0, int take = 0)
        {
            var q = Query();
            if (skip > 0) q = q.Skip(skip);
            if (take > 0) q = q.Take(skip);

            return (from r in q
             select new MessageUser
             {
                 Application = r.Application,
                 ToApplicationOrganization = r.ToApplicationOrganization,
                 ToApplicationUser = r.ToApplicationUser
             }).Distinct().ToList();
        }

        List<MessageUser> _customUserInfo = new List<MessageUser>();

        public void SetUsersInfo(List<MessageUser> infos)
        {
            if (infos == null) infos = new List<MessageUser>();
            _customUserInfo = infos;
        }

        public void AddUserInfo(MessageUser info)
        {
            if (_customUserInfo == null) _customUserInfo = new List<MessageUser>();
            _customUserInfo.Add(info);
        }


        public List<MessageStatus> Get(int skip=0, int take=0)
        {            
            var q = Query();
            if(skip > 0) q = q.Skip(skip);
            if(take > 0) q = q.Take(skip);

            List<Model.dpm_message> records = q.ToList();

            List<MessageStatus> ret = (from r in records select new MessageStatus(r)).ToList();

            if(_customUserInfo != null && _customUserInfo.Count > 0)
            {
                foreach (var item in ret)
                {
                    var info = (from r in _customUserInfo
                                where r.Application == item.Application
                                && r.ToApplicationUser == item.ToApplicationUser
                                && r.ToApplicationOrganization == item.ToApplicationOrganization
                                select r).FirstOrDefault();
                    if(info != null)
                    {
                        item.CustomCompany = info.CustomCompany;
                        item.CustomExtra1 = info.CustomExtra1;
                        item.CustomExtra2 = info.CustomExtra2;
                        item.CustomFirstName = info.CustomFirstName;
                        item.CustomLastName = info.CustomLastName;
                        item.CustomNumber = info.CustomNumber;
                        item.CustomRole = info.CustomRole;
                    }
                }
            }

            return ret;
        }
    }
}
