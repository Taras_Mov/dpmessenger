﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger
{
    public class AcknowledgementService
    {
        public void MarkAsRead(string id)
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            var rec = (from r in db.dpm_message where r.ID == id && r.AckDate == null select r).FirstOrDefault();
            if (rec != null)
            {
                rec.AckDate = DateTime.Now;
                db.SaveChanges();
            }
        }

    }
}
