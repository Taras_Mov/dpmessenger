﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger
{
    class Log
    {
        static object _lock = new object();
        public static void LogEvent(string type, bool success, string message)
        {
            if(!success)
            {
                LogToConsole(message, ConsoleColor.Red);
            }
            else
            {
                if (type == "PostMan")
                {
                    LogToConsole(message, ConsoleColor.Green);
                }
                else if (type == "CoreSend")
                {
                    LogToConsole(message, ConsoleColor.Green);
                }
                else if(type == "Core")
                {
                    LogToConsole(message, ConsoleColor.Cyan);
                }
                else
                {
                    LogToConsole(message, ConsoleColor.White);
                }
            }
            
        }

        public static void LogToConsole(string text, ConsoleColor c)
        {
            //This line was automatically generated don't remove
            //End of auto code

            lock (_lock)
            {
                Console.ForegroundColor = c;
                Console.WriteLine(text);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }
    }
}
