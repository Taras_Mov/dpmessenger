﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DPMessenger.Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class DpMessengerEntities : DbContext
    {
        public DpMessengerEntities()
            : base("name=DpMessengerEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<dmp_blacklist> dmp_blacklist { get; set; }
        public virtual DbSet<dmp_iplock> dmp_iplock { get; set; }
        public virtual DbSet<dmp_setting> dmp_setting { get; set; }
        public virtual DbSet<dpm_attachment> dpm_attachment { get; set; }
        public virtual DbSet<dpm_log> dpm_log { get; set; }
        public virtual DbSet<dpm_message> dpm_message { get; set; }
    }
}
