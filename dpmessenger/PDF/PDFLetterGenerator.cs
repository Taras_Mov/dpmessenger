﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using iTextSharp.text.pdf;

namespace DPMessenger.PDF
{
    public class PDFLetterGenerator
    {
        public string Title { get; set; }
        public string Text { get; set; }


        public byte[] GeneratePdfLetter()
        {
            MemoryStream ms = new MemoryStream();

            // Create a Document object
            var document = new Document(PageSize.A4, 50, 50, 25, 25);

            // Create a new PdfWriter object, specifying the output stream
            var writer = PdfWriter.GetInstance(document, ms);

            // Open the Document for writing
            document.Open();

            // Generate content
            var titleParagraph = new Paragraph(Title);
            titleParagraph.Alignment = Element.ALIGN_CENTER;
            document.Add(titleParagraph);

            var spaceParagraph = new Paragraph("\n");
            document.Add(spaceParagraph);


            var welcomeParagraph = new Paragraph(Text);
            document.Add(welcomeParagraph);
            // -->

            // Close the Document - this saves the document contents to the output stream
            document.Close();

            byte[] data = ms.ToArray();
            ms.Dispose();
            return data;
        }
    }
}
