﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.Security
{
    public class IPLock
    {
        public void RegisterWrongAccess(string ip)
        {
            Model.DpMessengerEntities db = new Model.DpMessengerEntities();
            db.dmp_iplock.Add(new Model.dmp_iplock
            {
                date = DateTime.Now,
                ip = ip
            });
            db.SaveChanges();
        }

        public bool IsIpLocked(string ip)
        {
            var conf = Configuration.Configuration.Read();

            if(conf != null && conf.MobileAttachments != null && conf.MobileAttachments.IPLockInMinutes>0)
            {
                DateTime d = DateTime.Now.Subtract(TimeSpan.FromMinutes(conf.MobileAttachments.IPLockInMinutes));
                Model.DpMessengerEntities db = new Model.DpMessengerEntities();

                if((from r in db.dmp_iplock where r.ip == ip && r.date>= d select r).Count() > conf.MobileAttachments.IPLockAfterErrors)
                {
                    return true;
                }
                
            }



            return false;
        }
    }
}
