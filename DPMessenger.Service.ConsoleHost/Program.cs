﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMessenger.Service.ConsoleHost
{
    class Program
    {
        public static void Main(string[] args)
        {

            Console.WriteLine(@"  _____  _____  __  __                                          ");
            Console.WriteLine(@" |  __ \|  __ \|  \/  |                                         ");
            Console.WriteLine(@" | |  | | |__) | \  / | ___  ___ ___  ___ _ __   __ _  ___ _ __ ");
            Console.WriteLine(@" | |  | |  ___/| |\/| |/ _ \/ __/ __|/ _ \ '_ \ / _` |/ _ \ '__|");
            Console.WriteLine(@" | |__| | |    | |  | |  __/\__ \__ \  __/ | | | (_| |  __/ |   ");
            Console.WriteLine(@" |_____/|_|    |_|  |_|\___||___/___/\___|_| |_|\__, |\___|_|   ");
            Console.WriteLine(@"                                                 __/ |          ");
            Console.WriteLine(@"                                                |___/           ");


            DpMessengerServiceImplementation service = new DpMessengerServiceImplementation();
            service.RunThreadedLoop();

            while (true)
            {
                Console.ReadLine();
            }
           

        }
    }
}
