﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Reply.aspx.cs" Inherits="Reply" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Merci - Thank you - Danke</title>
    <style>
        <%= MiniCss %>
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Panel ID="panelView" runat="server" CssClass="row" Visible="false">
                <div class="card warning">
                    <div class="section">
                        <h3 class="doc"><asp:Label runat="server" ID="lblConfirmation" /></h3>
                        <h4 class="doc"><asp:Label runat="server" ID="lblChoice" /></h4>
                        <p class="doc"><asp:Label runat="server" ID="lblThanks" /></p>
                    </div>
                </div>
            </asp:Panel>

        </div>
    </form>
</body>
</html>
