﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Unsubscribe.aspx.cs" Inherits="Unsubscribe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <asp:Panel runat="server" ID="panelForm">
            <p>
                Unsubscribe from messages. Se désinscrire des messages. Abbestellen von Nachrichten.
            </p>
            <div>
                <asp:TextBox runat="server" ID="tbxRecipient" ReadOnly="true" />
            </div>
            <div>
                <asp:Button runat="server" ID="btnUnsubscribe" OnClick="btnUnsubscribe_Click" Text="OK" />
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="panelDone" Visible="false">
            Unsubscription done. Désinscription faite. Abmeldung erledigt.
        </asp:Panel>
    </form>
</body>
</html>
