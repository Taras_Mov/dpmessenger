/*
SQLyog Community v13.1.2 (64 bit)
MySQL - 5.7.22-log : Database - dpmessenger
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dpmessenger` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dpmessenger`;

/*Table structure for table `dmp_blacklist` */

DROP TABLE IF EXISTS `dmp_blacklist`;

CREATE TABLE `dmp_blacklist` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Application` varchar(50) DEFAULT NULL,
  `MessageType` varchar(50) DEFAULT NULL,
  `Protocol` varchar(50) DEFAULT NULL,
  `Forbidden` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `dmp_iplock` */

DROP TABLE IF EXISTS `dmp_iplock`;

CREATE TABLE `dmp_iplock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Table structure for table `dmp_setting` */

DROP TABLE IF EXISTS `dmp_setting`;

CREATE TABLE `dmp_setting` (
  `Name` varchar(45) NOT NULL,
  `Type` varchar(45) NOT NULL,
  `Value` longtext,
  PRIMARY KEY (`Name`,`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `dpm_attachment` */

DROP TABLE IF EXISTS `dpm_attachment`;

CREATE TABLE `dpm_attachment` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MessageID` varchar(40) NOT NULL,
  `MimeType` varchar(100) DEFAULT NULL,
  `FileName` varchar(255) DEFAULT NULL,
  `Content` longblob,
  PRIMARY KEY (`ID`),
  KEY `fk_attachment_message` (`MessageID`),
  CONSTRAINT `fk_attachment_message` FOREIGN KEY (`MessageID`) REFERENCES `dpm_message` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;

/*Table structure for table `dpm_log` */

DROP TABLE IF EXISTS `dpm_log`;

CREATE TABLE `dpm_log` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MessageID` varchar(40) NOT NULL,
  `Date` datetime NOT NULL,
  `IsError` tinyint(1) DEFAULT NULL,
  `Message` text,
  PRIMARY KEY (`ID`),
  KEY `fk_dplog_dpmessage` (`MessageID`),
  CONSTRAINT `fk_dplog_dpmessage` FOREIGN KEY (`MessageID`) REFERENCES `dpm_message` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=722 DEFAULT CHARSET=latin1;

/*Table structure for table `dpm_message` */

DROP TABLE IF EXISTS `dpm_message`;

CREATE TABLE `dpm_message` (
  `ID` varchar(40) NOT NULL COMMENT 'is a guid',
  `ScheduleDate` datetime NOT NULL COMMENT 'when send the message',
  `Application` varchar(50) NOT NULL COMMENT 'use to identify your application',
  `MessageType` varchar(50) NOT NULL COMMENT 'use to identify your message type (welcome, renew, usernotify)',
  `Protocol` varchar(50) NOT NULL COMMENT 'EMAIL ? SMS ? CALL ?',
  `ToApplicationOrganization` varchar(50) NOT NULL COMMENT 'use to identify your application user''s organization or group',
  `ToApplicationUser` varchar(50) NOT NULL COMMENT 'use to identify your application''s user recipient (user id)',
  `ToPhoneNumber` varchar(20) DEFAULT NULL COMMENT 'recipient phone number',
  `Language` varchar(5) DEFAULT NULL,
  `FromEmail` varchar(45) DEFAULT NULL,
  `FromName` varchar(50) DEFAULT NULL,
  `ToEmail` varchar(250) DEFAULT NULL COMMENT 'recipients email number',
  `CcEmail` varchar(250) DEFAULT NULL,
  `ToMobileAppToken` text COMMENT 'recipients ios/android token',
  `Subject` text COMMENT 'email subject',
  `Message` longtext COMMENT 'message content',
  `SendDate` datetime DEFAULT NULL COMMENT 'when the message was sent',
  `SendServer` text,
  `SendingDuration` double DEFAULT NULL,
  `SendErrorRetries` int(11) DEFAULT NULL COMMENT 'if the message can not be sent',
  `AckDate` datetime DEFAULT NULL COMMENT 'when the message was acknowledged',
  `NeedAnswer` int(11) DEFAULT NULL COMMENT 'if the message need an answer',
  `PossibleAnswers` longtext COMMENT 'possible answers',
  `AnswerDate` datetime DEFAULT NULL COMMENT 'if the message answered',
  `AnswerClosed` int(11) DEFAULT '0',
  `AnswerValue` text COMMENT 'the answer value',
  `Canceled` int(11) DEFAULT '0' COMMENT 'If 1, cancel this message',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
