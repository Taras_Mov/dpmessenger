﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Unsubscribe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string token = Request.Params["q"];
        string type = Request.Params["t"]; // si non précisé, le type exact du message sera pris

        DPMessenger.Webservice.UnsubscriberManager mgr = new DPMessenger.Webservice.UnsubscriberManager();
        var x = mgr.GetInfo(token);
        if (x.CanUnsubscribe)
        {
            tbxRecipient.Text = x.AnonymousRecipient;
            panelForm.Visible = true;
        }
        else
        {
            panelForm.Visible = false;
        }
    }

    protected void btnUnsubscribe_Click(object sender, EventArgs e)
    {
        string token = Request.Params["q"];
        string type = Request.Params["t"]; // si non précisé, le type exact du message sera pris

        DPMessenger.Webservice.UnsubscriberManager mgr = new DPMessenger.Webservice.UnsubscriberManager();
        if(mgr.RegisterForbiddenRecipient(token, type))
        {
            panelDone.Visible = true;
            panelForm.Visible = false;
        }
    }
}