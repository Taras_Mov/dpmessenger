﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reply : System.Web.UI.Page
{
    public string MiniCss
    {
        get
        {
            return DPMessenger.Configuration.Configuration.GetMiniCss();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "";

        string messageId = Request.Params["q"];
        string value = Request.Params["v"];

        if (!String.IsNullOrWhiteSpace(messageId) && !String.IsNullOrWhiteSpace(value))
        {
            DPMessenger.Query.MessageInfo info = new DPMessenger.Query.MessageInfo(messageId);
            if (info.IsPossibleAnswer(value))
            {
                info.SetAnswer(value);
                lblConfirmation.Text = info.GetSentence("ReplyConfirmed");
                lblChoice.Text = info.AnswerText;
                lblThanks.Text = info.GetSentence("Thanks") + "!";

                panelView.Visible = true;
                Title = info.GetSentence("Thanks");
            }
            
        }
    }
}