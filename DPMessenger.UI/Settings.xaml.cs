﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DPMessenger.UI
{
    /// <summary>
    /// Logique d'interaction pour Settings.xaml
    /// </summary>
    public partial class Settings : UserControl
    {
        public Settings()
        {
            InitializeComponent();
            tbxHttp.Text = Properties.Settings.Default.DefaultUrl;
        }

        private void tbxSaveSettings_Click(object sender, RoutedEventArgs e)
        {
            string host = tbxHttp.Text;

            // GLOBAL CONFIGURATION
            DPMessenger.Configuration.Configuration config = new DPMessenger.Configuration.Configuration();
            config.SmtpConfiguration = new DPMessenger.Configuration.SmtpConfiguration();
            config.SmtpConfiguration.AutoBalance = true;
            config.SmtpConfiguration.TimeoutSeconds = 20;
            config.SmtpConfiguration.Servers = new List<DPMessenger.Configuration.SmtpServer>();
            config.SmtpConfiguration.Servers.Add(new DPMessenger.Configuration.SmtpServer
            {
                SmtpHost = "neptun.kreativmedia.ch",
                SmtpPort = 587,
                SmtpLogin = "no-reply@gesmobile.ch",
                SmtpPassword = "8^f25bGj",
                SmtpEnableSSL = true
            });
            // if multiple smtp
            /*config.SmtpConfiguration.Servers.Add(new DPMessenger.Configuration.SmtpServer
            {
                SmtpHost = "neptun.kreativmedia.ch",
                SmtpPort = 587,
                SmtpLogin = "no-reply@gesmobile.ch",
                SmtpPassword = "8^f25bGj",
                SmtpEnableSSL = true
            });*/
            config.ServiceConfiguration = new DPMessenger.Configuration.ServiceConfiguration();
            config.ServiceConfiguration.BatchNormalSize = 10;
            config.ServiceConfiguration.BatchErrorSize = 3;
            config.ServiceConfiguration.ErrorRetriesLimit = 4;
            config.ServiceConfiguration.RandomizeOrder = true;
            config.EmailAcknowledgement = new DPMessenger.Configuration.EmailAcknowledgement();
            config.EmailAcknowledgement.ImageTrackerUrl = host + "MarkAsRead.ashx?q={ID}";
            config.EmailAcknowledgement.UnsubscribeUrl = host + "Unsubscribe.aspx";
            config.EmailAcknowledgement.ReplyByClickUrl = host + "Reply.aspx?q={ID}&v={VALUE}";

            config.TwilioSettings = new DPMessenger.Configuration.TwilioSettings();
            config.TwilioSettings.AccountSid = "AC9f3aab235d49a3408ec987c7d90acf43";
            config.TwilioSettings.AuthToken = "4db9a2a05c836db0b2a8e9c60e77ae3d";
            config.TwilioSettings.SMSSenderNumber = "+41798073483";
            config.TwilioSettings.WhatsappSenderNumber = "+14155238886";
            config.TwilioSettings.FaxSenderNumber = "+41445051970";
            config.TwilioSettings.FaxDeliveryUrl = host + "Fax.ashx?q={ID}";
            config.TwilioSettings.VoiceSenderNumber = "+41445051970";
            config.TwilioSettings.VoiceDeliveryUrl = host + "Call.ashx?q={ID}";
            config.TwilioSettings.SMSStatusCallBackUrl = host + "SMSStatus.ashx?q={ID}";
            config.TwilioSettings.WhatsappSandboxNumber = "+14155238886";
            config.TwilioSettings.WhatsappSandboxCode = "join fun-apartment";
            config.TwilioSettings.WhatsappStatusCallBackUrl = host + "WhatsAppStatus.ashx?q={ID}";


            config.MobileAttachments = new DPMessenger.Configuration.MobileAttachmentsSettings();
            config.MobileAttachments.AttachmentsUrl = host + "Attachments.ashx?q={ID}";
            config.MobileAttachments.AttachmentsAvailableHours = 5;
            config.MobileAttachments.IPLockAfterErrors = 5;
            config.MobileAttachments.IPLockInMinutes = 15;

            config.InstantReplySettings = new DPMessenger.Configuration.InstantReplySettings();
            config.InstantReplySettings.TextMessageDelayInMinutes = 15;



            config.SpeachSettings = new DPMessenger.Configuration.SpeachSettings();
            config.SpeachSettings.SoundOpenerMp3Url = host + "assets/call/opener.mp3";
            config.SpeachSettings.Thanks.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Merci" });
            config.SpeachSettings.Thanks.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Thanks" });
            config.SpeachSettings.RepliesClick.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Merci d'indiquer votre choix cliquant sur le lien correspondant." });
            config.SpeachSettings.RepliesClick.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Pleaser, click the link corresponding to your choice." });
            config.SpeachSettings.RepliesMenu.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Merci d'indiquer votre choix en tapant le numéro souhaité en terminant par la touche dièze" });
            config.SpeachSettings.RepliesMenu.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Please, enter the number corresponding to your choice, followd by the pound sign" });
            config.SpeachSettings.RepliesMessage.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Merci d'indiquer votre choix en répondant à ce message par le numéro correspondant:" });
            config.SpeachSettings.RepliesMessage.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Please, reply this message by the number corresponding to your choice:" });
            config.SpeachSettings.ReplyConfirmed.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Choix confirmé: " });
            config.SpeachSettings.ReplyConfirmed.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Choice confirmed: " });
            config.SpeachSettings.ReplyNotFound.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Choix impossible." });
            config.SpeachSettings.ReplyNotFound.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Impossible choice." });
            config.SpeachSettings.WhatsappInvitation.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Notre application souhaite vous envoyer des notifications Whatsapp. Pour accepter, merci d'envoyer par whatsapp: \nMessage:\n$CODE$\nAu numéro:\n$NUMBER$\n\nOu cliquez sur le lien suivant pour créer le message et envoyez-le:\n$LINK$" });
            config.SpeachSettings.WhatsappInvitation.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Our application want to send you notifications via Whatsapp. To accept, thanks to send a whatsapp: \nMessage:\n$CODE$\nTo number:\n$NUMBER$\n\nOr click on the following link to create the message and send it:\n$LINK$" });
            config.SpeachSettings.ReplyBefore.Add(new DPMessenger.Configuration.VoiceItem { Language = "fr-FR", Text = "Réponse possible avant $DATE$." });
            config.SpeachSettings.ReplyBefore.Add(new DPMessenger.Configuration.VoiceItem { Language = "en-US", Text = "Answer possible before $DATE$." });

            DPMessenger.Configuration.Configuration.Save(config);
        }
    }
}
