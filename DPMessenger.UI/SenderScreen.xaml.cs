﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DPMessenger;

namespace DPMessenger.UI
{
    /// <summary>
    /// Logique d'interaction pour SenderScreen.xaml
    /// </summary>
    public partial class SenderScreen : UserControl
    {
        public SenderScreen()
        {
            InitializeComponent();
            tbxAnswersText.Text = Properties.Resources.DefaultChoices;
            tbxFiles.Text = Properties.Resources.DefaultAttachments;
            tbxAnswersText.MouseRightButtonUp += TbxAnswersText_MouseRightButtonUp;
            tbxFiles.MouseRightButtonUp += TbxFiles_MouseRightButtonUp;
        }

        private void TbxFiles_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            tbxFiles.Text = Properties.Resources.DefaultAttachments;
        }

        private void TbxAnswersText_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            tbxAnswersText.Text = Properties.Resources.DefaultChoices;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DPMessenger.NewMessage msg = new NewMessage();
            msg.Init(tbxApp.Text, tbxOrg.Text, tbxUser.Text, tbxMessage.Text);

            msg.ToEmail = tbxEmail.Text;
            msg.ToPhoneNumber = tbxPhone.Text;
            msg.FromEmail = "test@digitalpencorp.ch";
            msg.FromName = "DPMessenger TEST";

            if (cbxAppNotification.IsChecked == true) msg.Protocol = MessageProtocol.MOBILEAPPNOTIF;
            if (cbxCall.IsChecked == true) msg.Protocol = MessageProtocol.CALL;
            if (cbxEmail.IsChecked == true) msg.Protocol = MessageProtocol.EMAIL;
            if (cbxFax.IsChecked == true) msg.Protocol = MessageProtocol.FAX;
            if (cbxSMS.IsChecked == true) msg.Protocol = MessageProtocol.SMS;
            if (cbxWhatsApp.IsChecked == true) msg.Protocol = MessageProtocol.WHATSAPP;

            msg.Language = tbxLanguage.Text;
            msg.Subject = tbxSubject.Text;
            msg.Body = tbxMessageBody.Text;

            if (!String.IsNullOrWhiteSpace(tbxAnswersText.Text))
            {
                try
                {
                    msg.PossibleAnswers = DPMessenger.Answers.Answer.Deserialize(tbxAnswersText.Text);
                }
                catch
                {
                    MessageBox.Show("Invalid xml in Possible Answers");
                }

            }

            if (!String.IsNullOrWhiteSpace(tbxFiles.Text))
            {
                string[] files = tbxFiles.Text.Split('\n');
                files = (from r in files where !String.IsNullOrWhiteSpace(r) select r.Trim()).ToArray();
                foreach (var file in files)
                {
                    msg.AddAttachement(file);
                }
            }


            DPMessenger.DpPostMan pm = new DpPostMan();
            pm.SendNow(msg);
        }
    }
}
